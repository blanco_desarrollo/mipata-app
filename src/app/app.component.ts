import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Config, AlertController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

import { Profile } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { HomePage } from '../pages/home/home';
import { OnboardingPage } from '../pages/onboarding/onboarding';
import { MyPetsPage } from '../pages/my-pets/my-pets';
import { ListPage } from '../pages/list/list';
import { Searching } from '../pages/searching/searching';
import { Lost } from '../pages/lost/lost';
import { Clinic } from '../pages/clinic/clinic';
import { Doctor } from '../pages/doctor/doctor';

import { MimaskotUser } from '../models/mimaskot-user.model';
import {UserService} from "../services/user.service";
import { AuthService } from '../services/auth.service';
import { Pata } from '../pata';
import { FCM } from '@ionic-native/fcm';

import { environment } from "../environments/environment"


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = null;
  public staticUrl:string;

  pages: Array<{titleTranslationKey: string, title: string, component: any, selected: boolean}>;

  user: any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private translate: TranslateService,
    private config: Config,
    private storage: Storage,
    private service: Pata,
    private alertCtrl: AlertController,
    private userService:UserService,
    private authService:AuthService,
    private fcm: FCM,
    public menu: MenuController
  ){
    this.initializeApp();
    this.staticUrl = environment.staticUrl;
    // used for an example of ngFor and navigation
    this.pages = [
      { titleTranslationKey: 'APP_MENU.HOME', title: 'Inicio', component: HomePage, selected: true },
      { titleTranslationKey: 'APP_MENU.MY_PETS', title: 'Mis mascotas', component: MyPetsPage, selected: false },
      { titleTranslationKey: 'APP_MENU.LOOKING_FOR_PARTNERS', title: 'Buscando pareja', component: Searching, selected: false },
      { titleTranslationKey: 'APP_MENU.LOST_PETS', title: 'Mascotas perdidas', component: Lost, selected: false },
      { titleTranslationKey: 'APP_MENU.VETERINARY_CLINICS', title: 'Clínicas veterinarias', component: Doctor, selected: false }
    ];


    // Translate page title values
    this.pages.forEach((page) => {
      this.translate.get(page.titleTranslationKey).subscribe((translation) => {
        page.title = translation;
      })
    });

    this.userService.changeAvatar.subscribe((st) => {
      console.log(st);
      this.user.avatar = st;
    });


  }

  initializeApp() {

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      },2000);

      if (this.platform.is('cordova')) {

        this.platform.registerBackButtonAction(() => {
          if(this.menu.isOpen()){
             this.menu.close()
          }
          else if(this.nav.canGoBack()){
            this.nav.pop();
          }else{
            //don't do anything
          }
        });

        this.fcm.getToken().then(token=>{
          //alert('push:'+ token);
          this.userService.setPush(token);
          this.authService.setPush(token);
        })

        this.fcm.onNotification().subscribe(data=>{
          //alert('Push:' + JSON.stringify(data));
          if(data.wasTapped){
            console.log("Received in background");
          } else {
            console.log("Received in foreground");
          };
        })

        this.fcm.onTokenRefresh().subscribe(token=>{
          //alert('push:'+ token);
          this.userService.setPush(token);
          this.authService.setPush(token);
        });
      }

      this.authService.loginOk.subscribe(data => {
        if (data.profile.avatar == null || data.profile.avatar == "") {
          data.profile.avatar = "assets/img/default/avatar.png";
        }
        this.user = data.profile;

        if (data.profile.avatar != "" && data.profile.avatar != null && data.profile.avatar != 'assets/img/default/avatar.png') {
          this.user.avatar = this.staticUrl + this.user.avatar.replace('/public/','');
        }
        this.authService.sendPushToServer(this.user.id);
      });

      this.storage.get("MP-FirstTime").then((val) => {
        if (val == true) {
          this.storage.get("MP-FirstTime").then((val) => {
            this.storage.get("token").then((token) => {
              if (token) {

                this.userService.getProfile().subscribe((result:any)=>{
                    this.user = result;
                    this.userService.getId(result.id).subscribe((ok) => {
                      this.user = ok;
                      if (this.user.avatar != null && this.user.avatar != "") {
                        this.user.avatar = this.staticUrl + this.user.avatar.replace('/public/','');
                      }
                      else {
                        this.user.avatar = "assets/img/default/avatar.png";
                      }

                      this.userService.sendPushToServer();

                      this.rootPage = HomePage;

                    }, (err) => {
                      this.service.logError(null, "No fue posible recuperar tu perfil. Por favor accede nuevamente.");
                      this.rootPage = LoginPage;
                    });


                }, (err) => {
                  this.rootPage = LoginPage;
                });

              } else {
                this.rootPage = LoginPage;
              }
            });
          });
        }
        else {
          this.rootPage = OnboardingPage;
        }
      })

    });

  }

  profile() {
    this.nav.setRoot(Profile);
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    /*
    this.pages.forEach((p) => {
      p.selected = false;
    });
    page.selected = true;
    */
  }

  closeSession() {
  let alert = this.alertCtrl.create({
    title: 'Hasta pronto',
    message: 'Deseas salir de tu cuenta',
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {}
      },
      {
        text: 'Confirmar',
        handler: () => {
          this.storage.remove("token");
          this.nav.setRoot(LoginPage);
        }
      }
    ]
  });
  alert.present();
  }
}


export interface IUser {
  avatar: string;
  email: string;
  name: string;
}

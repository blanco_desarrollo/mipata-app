import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import { BaseService } from "./base.service"

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import {environment} from "../environments/environment";

@Injectable()
export class NewsService extends BaseService{

  constructor(public http: HttpClient, public storage: Storage) {
    super(http, storage);
  }

  geList(page, perPage) {
    //return this.get('news/');
    let apiNewsUrl = environment.newsApi + 'noticias/?page='+page+'&per_page='+perPage+'&status=publish';
    return  this.http.get(apiNewsUrl, {
      params: null
    });
  }

  getFeatured(id) {
    let apiMediaUrl = environment.newsApi + 'media/';
    return  this.http.get(apiMediaUrl + id, {
      params: null
    });
  }

}

import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';

import { environment } from '../environments/environment';

@Injectable()
export class AuthService  {

  @Output() loginOk = new EventEmitter<any>();
  public push: string = '';

  constructor(public http: HttpClient, private storage: Storage, private device: Device) {}

  /** login */
  public login(params: { email: string, password: string }) {
    let req = this.http.post(environment.apiUrl+'auth/login', params);

    req.subscribe(data => {
      this.loginOk.emit(data);
    });
    return req;
  }

  /** signup */
  public signup(params: { name: string, email: string, phone:string, password: string }) {
    let req = this.http.post(environment.apiUrl+'users/register', params);
    return req;
  }

  public setToken(token) {
    this.storage.set('token', token);
  }


  /** verification sms */
  public verifyPhone(params: { email: string, code:string }) {
    let req = this.http.post(environment.apiUrl+'users/verification', params);
    return req;
  }

  setPush(token) {
    this.push = token;
  }

  sendPushToServer(id:number) {
    this.http.post(environment.apiUrl+'users/device', {
      id: id,
      token: this.push,
      os: this.device.platform,
      osversion: this.device.version,
      appversion: environment.version
    }).subscribe((ok) => {
      console.log('push ok', ok);
    }, (err) => {
      console.log('push err', err);
    });
  }

  /** logout */
  logout() {}
}

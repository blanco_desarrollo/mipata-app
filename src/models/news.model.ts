import { OptEntity } from '@option/core';

export class News extends OptEntity {
  image: string;
  title: any;
  created_at: Date;
  description: string;
  body: string;
  link:string;
  featured_media: string;

  protected decode(jsonObject: object): void {
    this.image = jsonObject['image'];
    this.title = jsonObject['title'];

    if (jsonObject['date']) {
      this.created_at = new Date(jsonObject['date']);
      this.normalizeDateToUsersLocale(this.created_at);
    }
  }

  getFormEntityName(): string {
    return 'news';
  }

  private normalizeDateToUsersLocale(date: Date){
    date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
  }
}

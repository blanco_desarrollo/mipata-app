import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { HomePage } from '../home/home';
import { Pata } from '../../pata';

import { AuthService } from '../../services/auth.service';
import { SignupPage } from "../signup/signup";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  public loading: any;

  public login: { email:string, password:string } = {
    email: '',
    password:''
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: Pata,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    private auth: AuthService
  ){
  	this.login = {
  		email: '',
  		password: ''
  	};
  }

  next() {
  	let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  	if (this.login.email == "" || !emailRegex.test(this.login.email)) {
  		this.service.logError({}, "Email no puede estar vacío o es incorrecto");
  	}
  	else if (this.login.password == "") {
  		this.service.logError({}, "La contraseña no puede estar vacía");
  	}
  	else {
      this.loading = this.loadingCtrl.create();
      this.loading.present();

      this.auth.login(this.login).subscribe(
        (data: ILogin) => {
          this.loading.dismiss();
          if(data.token){
            this.auth.setToken(data.token);
            this.storage.set("MP-Profile", data.profile);
            this.navCtrl.setRoot(HomePage);
          }else{
            this.service.logError({}, "Email o Contraseña inválida");
          }
        },
        err => {
          this.loading.dismiss();
          if (navigator.onLine) {
            this.service.logError({}, "Email o Contraseña inválida");
          } else {
            this.service.logError({}, "No fue posible conectar al servidor. Verifica tu conexión a internet");
          }
          
        }
      );

  	}

  }

  gotoSignup(){
    this.navCtrl.push(SignupPage);
  }

}

/** Crear y mover a archivo de Interfaces */
export interface ILogin {
  token: string;
  status: boolean;
  profile: any;
}

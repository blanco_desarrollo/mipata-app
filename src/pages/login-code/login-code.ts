import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { HomePage } from '../home/home';
import { Pata } from '../../pata';
import {AuthService} from "../../services/auth.service";

/**
 * Generated class for the LoginCodePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-code',
  templateUrl: 'login-code.html',
})
export class LoginCodePage  {
  @ViewChild('uno') inp1 ;
  @ViewChild('dos') inp2 ;
  @ViewChild('tres') inp3 ;
  @ViewChild('cuatro') inp4 ;
  requestInProgress: boolean = false;
  code: any = null;
  params: {email:string, code:string} ={
    email:'',
    code: ''
  };
  login: any = null;
  token: any = null;
  perfil: any = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: Pata,
    public storage: Storage,
    private auth: AuthService
  ){
    //this.params.email = navParams.get('email');
    this.login = navParams.get('login');
    this.perfil = navParams.get('perfil');
    this.token = navParams.get('token');
    this.code = {
      uno: '',
      dos: '',
      tres: '',
      cuatro: ''
    };
  }


  getCode() {
      return this.code.uno+this.code.dos+this.code.tres+this.code.cuatro;
  }

  resetInputs(){
    this.code.uno = '';
    this.code.dos = '';
    this.code.tres = '';
    this.code.cuatro = '';
  }

  valuechange(value, i) {
    i = parseInt(i);
    console.log('valuechange '+value+','+i);
    switch(i) {
      case 1: {
        if(value != "" || value > 0)
          setTimeout(() => { document.getElementById('e2').querySelector("input").focus(); },300);
        break;
      }
      case 2: {
        if(value != "" || value > 0)
          setTimeout(() => { document.getElementById('e3').querySelector("input").focus(); },300);
        break;
      }
      case 3: {
        if(value != "" || value > 0)
          setTimeout(() => { document.getElementById('e4').querySelector("input").focus(); },300);
        break;
      }
      case 4: {
        if(value != "" || value > 0)
          this.accept();
        break;
      }
      default: {
        break;
      }
    }
  }

  accept() {
    this.requestInProgress = true;
    this.params.code = this.getCode();
    this.auth.verifyPhone({
      email: this.login.email,
      code: this.getCode()
    }).subscribe(
      (result: Response)  => {
        if(result.status){
          this.auth.login({
            email: this.login.email,
            password: this.login.password
          }).subscribe(
            (data: any) => {
              if (data.token) {
                this.auth.setToken(data.token);
                this.storage.set("MP-Profile", data.profile);
                this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: 'forward'});

              } else{
                this.requestInProgress = false;
                this.service.logError({}, "Email o Contraseña inválida");
              }
            },
            err => {
              this.requestInProgress = false;
              this.service.logError({}, "Email o Contraseña inválida");
            }
          );

        } else{
          this.requestInProgress = false;
          this.resetInputs();
          this.service.logError({}, "Código de verificación invalido", function() {
            document.getElementById('e1').querySelector("input").focus();
          });
        }
      },
      err => {
        this.requestInProgress = false;
        this.service.logError({}, JSON.stringify(err));
      }
    );
  }

  goBack() {
    this.navCtrl.pop();
  }

}

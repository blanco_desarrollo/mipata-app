import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { LoginCodePage } from '../login-code/login-code';
import { Pata } from '../../pata';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})

export class SignupPage {

  public login: any = null;
  public perfil: any = null;
  public token: string = '';
  public loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: Pata,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    private auth: AuthService
  ){
  	this.login = {
  		name: '',
  		email: '',
  		phone: '',
      password: '',
      repassword: ''
  	};
  }

  gotoCode(){
    this.navCtrl.push(LoginCodePage, this.login);
  }

  next() {
  	let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  	if (this.login.name == "") {
  		this.service.logError({}, "Nombre no puede estar vacío");
  	}
  	else if (this.login.email == "" || !emailRegex.test(this.login.email)) {
  		this.service.logError({}, "Email no puede estar vacío o es incorrecto");
  	}
  	else if (this.login.phone == "" || this.login.phone.length != 9) {
  		this.service.logError({}, "Número de teléfono celular es incorrecto, utilice 9 digitos");
  	}
    else if (this.login.password == "" || this.login.password.length < 6) {
      this.service.logError({}, "La contraseña debe tener al menos 6 caracteres");
    }
    else if (this.login.password != this.login.repassword) {
      this.service.logError({}, "Las contraseñas no coinciden ");
    }
  	else {

      this.loading = this.loadingCtrl.create();
      this.loading.present();

  	  this.auth.signup(this.login).subscribe(
        (data: ISignup) => {
          this.loading.dismiss();
          if(data.token){
            //this.auth.setToken(data.token);
            //this.storage.set("MP-Profile", data.profile);
            this.perfil = data.profile;
            this.token = data.token;
            this.navCtrl.push(LoginCodePage, {
              login: this.login,
              perfil: this.perfil,
              token: this.token
            });
          }else{
            this.service.logError({}, "Email o Teléfono ya está en uso, si no has recibido el SMS intenta activar tu cuenta mediante el correo electrónico que enviamos.");
          }
        },
        err => {
          this.loading.dismiss();
          this.service.logError({}, "Email o Contraseña inválida");
        }
      );
  	}
  }

  gotoLogin(){
    this.navCtrl.pop();
  }

}


export interface ISignup {
  token: string;
  status: boolean;
  profile: any;
}

webpackJsonp([0],{

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    "production": true,
    "apiUrl": "http://138.197.196.64/api/",
    "staticUrl": "http://138.197.196.64/",
    "newsApi": "http://138.68.234.226/wp-json/wp/v2/",
    "token": "5981efe0d6f33",
    "branchId": 262,
    "version": "1.0"
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Pata; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_forkJoin__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_forkJoin___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_forkJoin__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modals_ok_ok__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modals_err_err__ = __webpack_require__(284);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var Pata = (function () {
    function Pata(alertCtrl, storage, http, modalCtrl) {
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.http = http;
        this.modalCtrl = modalCtrl;
        this.send = [];
        this.backup = [];
    }
    Pata.prototype.logError = function (o, msg, cb) {
        /*
        if (!msg) { msg = 'Ocurrio un error al procesar su solicitud. Intente más tarde'; }
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: msg,
          buttons: ['OK']
        });
        alert.present();
        if (o !== null) { }
        */
        if (!msg) {
            msg = "Ha ocurrido un error, intente nuevamente";
        }
        var errModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__modals_err_err__["a" /* ModalERR */], { msg: msg });
        errModal.present();
        errModal.onDidDismiss(function (data) {
            if (cb) {
                cb();
            }
        });
    };
    Pata.prototype.showMsg = function (msg) {
        var alert = this.alertCtrl.create({
            title: '',
            cssClass: 'alertSuccess',
            subTitle: msg,
            buttons: ['Aceptar']
        });
        alert.present();
    };
    Pata.prototype.showOk = function (msg) {
        if (!msg) {
            msg = "La acción fue realizada exitosamente!";
        }
        var okModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modals_ok_ok__["a" /* ModalOK */], { msg: msg });
        okModal.present();
    };
    Pata.prototype.headers = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Headers */]();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.perfil.token);
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["h" /* RequestOptions */]({ headers: headers });
        return options;
    };
    return Pata;
}());
Pata = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
], Pata);

//# sourceMappingURL=pata.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__signup_signup__ = __webpack_require__(287);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, service, loadingCtrl, storage, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.auth = auth;
        this.login = {
            email: '',
            password: ''
        };
        this.login = {
            email: '',
            password: ''
        };
    }
    LoginPage.prototype.next = function () {
        var _this = this;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.login.email == "" || !emailRegex.test(this.login.email)) {
            this.service.logError({}, "Email no puede estar vacío o es incorrecto");
        }
        else if (this.login.password == "") {
            this.service.logError({}, "La contraseña no puede estar vacía");
        }
        else {
            this.loading = this.loadingCtrl.create();
            this.loading.present();
            this.auth.login(this.login).subscribe(function (data) {
                _this.loading.dismiss();
                if (data.token) {
                    _this.auth.setToken(data.token);
                    _this.storage.set("MP-Profile", data.profile);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
                }
                else {
                    _this.service.logError({}, "Email o Contraseña inválida");
                }
            }, function (err) {
                _this.loading.dismiss();
                if (navigator.onLine) {
                    _this.service.logError({}, "Email o Contraseña inválida");
                }
                else {
                    _this.service.logError({}, "No fue posible conectar al servidor. Verifica tu conexión a internet");
                }
            });
        }
    };
    LoginPage.prototype.gotoSignup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__signup_signup__["a" /* SignupPage */]);
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content scrollX="false" scrollY="false" scroll="false" class="noscroll">\n	<div class="background"></div>\n	<div class="overlay"></div>\n	<ion-scroll scrollX="false" scrollY="true">\n	  <img class="logo" src="assets/img/logo_white.svg" alt="">\n	  <div class="welcome">{{ \'LOGIN.WELCOME\' | translate }}</div>\n	  <div class="description">{{ \'LOGIN.DESCRIPTION\' | translate }}</div>\n	  <div class="listWrapper" scroll="false">\n	    <ion-list>\n	      <ion-item>\n	        <ion-input type="email" [(ngModel)]="login.email" placeholder="{{ \'LOGIN.EMAIL\' | translate }}"></ion-input>\n	      </ion-item>\n	      <ion-item>\n	        <ion-input type="password" [(ngModel)]="login.password" placeholder="{{ \'LOGIN.PASSWORD\' | translate }}"></ion-input>\n	      </ion-item>\n	    </ion-list>\n	  </div>\n	  <div class="buttonWrapper">\n	    <button class="buttonPinkOrange" ion-button round full (click)="next()">{{ \'LOGIN.NEXT\' | translate }}</button>\n	  </div>\n	  <div class="termsAndConditions">\n	    {{ \'LOGIN.ACCEPT_TERMS_AND_CONDITIONS_1\' | translate }}\n	    {{ \'LOGIN.TERMS_OF_USE\' | translate }}\n	    {{ \'LOGIN.ACCEPT_TERMS_AND_CONDITIONS_2\' | translate }}\n	    {{ \'LOGIN.PRIVACY_POLICY\' | translate }}\n	    {{ \'LOGIN.ACCEPT_TERMS_AND_CONDITIONS_3\' | translate }}\n	  </div>\n	  <div class="buttonWrapper">\n	    <button class="buttonPinkOrange small signup" ion-button round  (click)="gotoSignup()">{{ \'LOGIN.TO_SIGNUP\' | translate }}</button>\n	  </div>\n	</ion-scroll>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Owner; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_email_composer__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__ = __webpack_require__(294);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var Owner = (function () {
    function Owner(navCtrl, storage, navParams, popoverCtrl, loadingCtrl, service, userService, emailComposer, callNumber) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.userService = userService;
        this.emailComposer = emailComposer;
        this.callNumber = callNumber;
        this.isBlurred = false;
        this.loaded = false;
        this.owner = 0;
        this.staticUrl = '';
        this.regiones = [];
        this.distritos = [];
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].staticUrl;
        var loading = this.loadingCtrl.create({
            content: 'recuperando...'
        });
        loading.present();
        var getOp = this.userService.getId(this.navParams.get("owner"));
        getOp.subscribe(function (ok) {
            loading.dismiss();
            _this.me = ok;
            if (_this.me.avatar != null && _this.me.avatar != "") {
                _this.me.avatar = _this.me.avatar.replace('/public/', '');
            }
            else {
                //this.me.avatar = "assets/img/default/avatar.png";
            }
            _this.me.region_name = "";
            _this.me.distrito_name = "";
            _this.me.genero = "No especifica";
            if (parseInt(_this.me.genero) == 1) {
                _this.me.genero = "Masculino";
            }
            if (parseInt(_this.me.genero) == 2) {
                _this.me.genero = "Femenino";
            }
            _this.userService.region().subscribe(function (ok) {
                _this.regiones = ok;
                for (var i = 0; i < _this.regiones.length; i++) {
                    if (_this.regiones[i].id == _this.me.region) {
                        _this.me.region_name = _this.regiones[i].region;
                        break;
                    }
                }
            });
            if (_this.me.region.toString() != "0") {
                _this.userService.distrito(_this.me.region).subscribe(function (ok) {
                    _this.distritos = ok;
                    for (var i = 0; i < _this.distritos.length; i++) {
                        if (_this.distritos[i].id == _this.me.district) {
                            _this.me.distrito_name = _this.distritos[i].distrito;
                            break;
                        }
                    }
                });
            }
            _this.loaded = true;
        }, function (error) {
            loading.dismiss();
            _this.service.logError(null, "No fue posible recuperar los datos, intente más tarde");
            console.log(error);
        });
    }
    /** Call to Owner */
    Owner.prototype.call = function (phoneNumber) {
        this.callNumber.callNumber(phoneNumber, true)
            .then(function () { return console.log('Launched dialer!'); })
            .catch(function () { return console.log('Error launching dialer'); });
    };
    /** Send Email to Owner */
    Owner.prototype.sendEmail = function (emailOwner) {
        var _this = this;
        this.emailComposer.isAvailable().then(function (available) {
            if (true) {
                var email = {
                    to: emailOwner,
                    subject: _this.me.name + ' encontré a tu Mascota',
                    body: 'Hola ' + _this.me.name + ', ¡Encontré a tu Mascota perdida!',
                    isHtml: true
                };
                // Send a text message using default options
                _this.emailComposer.open(email);
            }
            else {
                // this.service.logError({}, 'Debes tener una cuenta de email registada en el teléfono antes de enviar el email');
            }
        }, function (error) {
            console.log('send.email.error: ' + JSON.stringify(error));
        });
    };
    return Owner;
}());
Owner = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-owner',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/owner/owner.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <span class="icon-menu"></span>\n    </button>\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'blurred\' : isBlurred}">\n\n  <div *ngIf="loaded">\n    <div class="mediaSlideContainer"><ion-slides pager slidesPerView="1">\n      <div class="media" [style.background-image]="\'url(\' + (me.avatar != \'\' && me.avatar != null ? staticUrl+me.avatar : \'assets/img/default/avatar.png\' ) + \')\'" style="background-size: cover;"></div>\n      </ion-slides>\n    </div>\n    <div class="name">\n      {{ me.name }}\n    </div>\n    <div class="welcome">\n      Información del dueño\n    </div>\n\n    <div class="formContainer">\n      <ion-item>\n        <ion-label fixed>Nombre</ion-label>\n        <ion-input type="text" [(ngModel)]="me.name" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Email</ion-label>\n        <ion-input type="text" [(ngModel)]="me.email" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Nro. Teléfono</ion-label>\n        <ion-input type="text" [(ngModel)]="me.phone" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Dirección</ion-label>\n        <ion-input type="text" [(ngModel)]="me.address" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Región</ion-label>\n        <ion-input type="text" [(ngModel)]="me.region_name" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Distrito</ion-label>\n        <ion-input type="text" [(ngModel)]="me.distrito_name" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Género</ion-label>\n        <ion-input type="text" [(ngModel)]="me.genero" [disabled]="true"></ion-input>\n      </ion-item>\n    </div>\n\n    <div class="buttonWrapper" *ngIf="me.phone.length > 3">\n      <button class="buttonPinkOrange" (click)="call(me.phone)" ion-button round>Llamar a {{ me.name }}</button>\n    </div>\n\n    <div class="buttonWrapper" *ngIf="me.email.length > 3">\n      <button class="buttonPinkOrange" (click)="sendEmail(me.email)" ion-button round>Enviar un email a {{ me.name }}</button>\n    </div>\n\n  </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/owner/owner.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* PopoverController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_3__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_email_composer__["a" /* EmailComposer */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__["a" /* CallNumber */]])
], Owner);

//# sourceMappingURL=owner.js.map

/***/ }),

/***/ 171:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 171;

/***/ }),

/***/ 214:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 214;

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PetService = (function (_super) {
    __extends(PetService, _super);
    function PetService(http, storage) {
        var _this = _super.call(this, http, storage) || this;
        _this.http = http;
        _this.storage = storage;
        return _this;
    }
    PetService.prototype.getLast = function () {
        return this.get('pets/last/');
    };
    PetService.prototype.getId = function (id) {
        return this.get('pets/' + id + '/');
    };
    PetService.prototype.update = function (id, data) {
        return this.post('pets/' + id, {
            "_method": "PUT",
            name: data.name,
            type: data.type,
            breed: data.breed,
            size: data.size,
            sex: data.sex,
            color: data.color,
            birthday: data.birthday,
            identifier: data.code,
            code: data.code,
            description: ''
        });
    };
    PetService.prototype.uniqueQR = function (code) {
        return this.post('pets/checkqr', {
            code: code
        });
    };
    PetService.prototype.getMascotByQR = function (code) {
        return this.post('missing-pet/found', {
            code: code
        });
    };
    /** Get My Missing Pet  */
    PetService.prototype.getMyMissingPet = function (idPet) {
        return this.get('pets/missing/' + idPet, {});
    };
    /** El dueño marca como mascota encontrada */
    PetService.prototype.petFound = function (code) {
        return this.post('missing-pet/found', {
            code: code
        });
    };
    /** Notificar al usuario de la mascota perdida */
    PetService.prototype.notifyFoundPetFromQr = function (body) {
        return this.post('missing-pet/seen', body);
        /*
        * {
          "code":body.code,
          "lat_seen":body.lat_seen,
          "lng_seen":body.lng_seen,
          "user_id":body.user_id
        }*/
    };
    /** saber si el QR pertenece a una mascota perdida */
    PetService.prototype.getLostPetByQR = function (code) {
        return this.get('missing-pet/code/' + code);
    };
    PetService.prototype.setCode = function (id, code) {
        return this.post('pets/code', {
            id: id,
            code: code
        });
    };
    PetService.prototype.add = function (id, data) {
        return this.post('pets/', {
            user_id: id,
            name: data.name,
            type: data.type,
            breed: data.breed,
            size: data.size,
            sex: data.sex,
            color: data.color,
            birthday: data.birthday,
            identifier: data.code,
            code: data.code,
            description: ''
        });
    };
    PetService.prototype.setState = function (id, state, obj) {
        if (state == 'searching') {
            return this.post('pets/available/', {
                id: id,
                available: 1
            });
        }
        if (state == 'nosearching') {
            return this.post('pets/available/', {
                id: id,
                available: 0
            });
        }
        else if (state == 'remove') {
            return this.delete('pets/' + id);
        }
        else if (state == 'lost') {
            return this.post('missing-pet/', {
                pet_id: id,
                lat: obj.lat,
                lng: obj.lng
            });
        }
        else if (state == 'found') {
            return this.post('missing-pet/found/', {
                id: id
            });
        }
    };
    /** Notificar al dueño */
    PetService.prototype.notifyOwner = function (id, myPetId) {
        return this.post('applications/' + id, {
            requester_pet_id: myPetId
        });
    };
    PetService.prototype.accept = function (id) {
        return this.get('applications/accept/' + id);
    };
    PetService.prototype.reject = function (id) {
        return this.get('applications/reject/' + id);
    };
    PetService.prototype.searchAvailable = function (data) {
        return this.post('pets/search/', data);
    };
    PetService.prototype.searchLost = function (data) {
        return this.post('missing-pet/search/', data);
    };
    PetService.prototype.getMyLove = function (data) {
        return this.post('pets/search/', data);
    };
    PetService.prototype.getMyLoveRecived = function (id) {
        return this.get('applications/received/' + id);
    };
    PetService.prototype.mainImage = function (id, index) {
        return this.post('pets/image/main/', {
            pet_id: id,
            index: index
        });
    };
    PetService.prototype.deleteImage = function (id, index) {
        return this.post('pets/image/delete/', {
            pet_id: id,
            index: index
        });
    };
    PetService.prototype.especie = function () {
        return this.get('species');
    };
    PetService.prototype.raza = function (raza) {
        return this.get('species/' + raza + '/breeds');
    };
    PetService.prototype.region = function () {
        return this.get('region');
    };
    PetService.prototype.distrito = function (region) {
        return this.get('region/' + region + '/district');
    };
    PetService.prototype.color = function () {
        return this.get('color');
    };
    PetService.prototype.size = function () {
        return this.get('size');
    };
    PetService.prototype.sex = function () {
        return this.get('petsex');
    };
    return PetService;
}(__WEBPACK_IMPORTED_MODULE_3__base_service__["a" /* BaseService */]));
PetService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], PetService);

//# sourceMappingURL=pet.service.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Profile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__popovers_profile_media_profile_media__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_date_picker__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__change_password_change_password__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__home_home__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var Profile = (function () {
    function Profile(navCtrl, storage, navParams, popoverCtrl, loadingCtrl, service, userService, datePicker, modalCtrl, datePipe) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.userService = userService;
        this.datePicker = datePicker;
        this.modalCtrl = modalCtrl;
        this.datePipe = datePipe;
        this.isBlurred = false;
        this.loaded = false;
        this.regiones = [];
        this.distritos = [];
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].staticUrl;
        this.storage.get("MP-Profile").then(function (val) {
            _this.userService.getId(val.id).subscribe(function (ok) {
                _this.me = ok;
                _this.loaded = true;
                if (_this.me.avatar != null && _this.me.avatar != "") {
                    _this.me.avatar = _this.staticUrl + _this.me.avatar.replace('/public/', '');
                }
                else {
                    _this.me.avatar = "assets/img/default/avatar.png";
                }
                _this.changeAvatar(_this.me.avatar);
                if (_this.me.region.toString() != "0") {
                    _this.refreshDistrito(_this.me.region);
                }
            }, function (err) {
                _this.service.logError(null, "No fue posible recuperar tu perfil. Verifica la disponibilidad de internet");
            });
        });
        this.userService.region().subscribe(function (ok) {
            _this.regiones = ok;
        });
    }
    Profile.prototype.refreshDistrito = function (e) {
        var _this = this;
        this.userService.distrito(e).subscribe(function (ok) {
            _this.distritos = ok;
        });
    };
    Profile.prototype.save = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'guardando...'
        });
        loading.present();
        this.storage.get('MP-Profile').then(function (val) {
            var updateOperation = _this.userService.update({
                "_method": "PUT",
                name: _this.me.name,
                last_name: _this.me.last_name,
                birthday: _this.me.birthday,
                address: _this.me.address,
                phone: _this.me.phone,
                email: _this.me.email,
                region: _this.me.region,
                district: _this.me.district,
                gender: _this.me.gender
            });
            updateOperation.subscribe(function (ok) {
                loading.dismiss();
                if (ok.status == 1) {
                    _this.service.showOk();
                }
                else {
                    _this.service.logError(null, "No fue posible guardar sus datos, intente nuevamente");
                }
            }, function (error) {
                loading.dismiss();
                _this.service.logError(null, "No fue posible guardar sus datos, intente nuevamente");
                console.log(error);
            });
        });
    };
    Profile.prototype.removeBlur = function () {
        this.isBlurred = false;
    };
    Profile.prototype.presentMediaOptionsPopover = function (event) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_5__popovers_profile_media_profile_media__["a" /* ProfileMedia */]);
        popover.present({
            ev: event
        });
        popover.onDidDismiss(function (change) {
            if (change) {
                _this.storage.get("MP-Profile").then(function (val) {
                    _this.me = val;
                    _this.loaded = true;
                    if (_this.me.avatar != null && _this.me.avatar != "") {
                        _this.me.avatar = _this.me.avatar.replace('/public/', '');
                    }
                    _this.changeAvatar(_this.me.avatar);
                });
            }
            _this.removeBlur();
        });
        this.isBlurred = true;
    };
    Profile.prototype.changeAvatar = function (avatar) {
        this.userService.changeAvatar.emit(avatar);
    };
    /** Birthday Date Picker */
    Profile.prototype.openDatepicker = function () {
        var _this = this;
        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            var d = _this.datePipe.transform(date, 'dd/MM/yyyy');
            console.log('Got date: ', d);
            _this.me.birthday = d;
        }, function (err) {
            console.log('Error occurred while getting date: ', err);
        });
    };
    Profile.prototype.changePassword = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__change_password_change_password__["a" /* ChangePasswordPage */], {});
        profileModal.onDidDismiss(function (data) {
            if (data) {
                _this.service.showOk();
            }
            else {
                //this.service.showOk();
            }
        });
        profileModal.present();
    };
    Profile.prototype.formatDate = function (date) {
        return date;
    };
    Profile.prototype.gotoHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__home_home__["a" /* HomePage */]);
    };
    return Profile;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])('datepicker'),
    __metadata("design:type", Object)
], Profile.prototype, "datepicker", void 0);
Profile = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button icon-only (click)="gotoHome()" class="my-style-for-modal">\n      <ion-icon class="ionicons" name="arrow-back"></ion-icon>\n    </button>\n    <ion-buttons end>\n    <button (click)="presentMediaOptionsPopover($event)" class="buttonPinkOrange camera">\n      <ion-icon ios="ios-camera-outline" md="ios-camera-outline"></ion-icon>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'blurred\' : isBlurred}">\n\n  <div *ngIf="loaded">\n    <div class="mediaSlideContainer">\n        <div class="media" [style.background-image]="\'url(\' + (me.avatar != \'\' && me.avatar != null ? me.avatar : \'assets/img/default/avatar.png\' ) + \')\'" style="background-size: cover;"></div>\n    </div>\n    <div class="name">\n      {{ me.name }}\n    </div>\n    <div class="welcome">\n      Información de mi cuenta\n    </div>\n\n    <div class="formContainer">\n      <ion-item>\n        <ion-label fixed>Nombre</ion-label>\n        <ion-input type="text" [(ngModel)]="me.name"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Apellido</ion-label>\n        <ion-input type="text" [(ngModel)]="me.last_name"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Cumpleaños</ion-label>\n        <ion-input [readonly]="true" #datepicker type="text" [(ngModel)]="me.birthday"  (click)="openDatepicker()"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Email</ion-label>\n        <ion-input type="text" [(ngModel)]="me.email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Nro. Teléfono</ion-label>\n        <ion-input type="tel" maxlength="9" [(ngModel)]="me.phone"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Dirección</ion-label>\n        <ion-input type="text" [(ngModel)]="me.address"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Región</ion-label>\n        <ion-select [(ngModel)]="me.region" (ionChange)="refreshDistrito($event)">\n          <ion-option *ngFor="let region of regiones" [value]="region.id">{{region.region}}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Distrito</ion-label>\n        <ion-select [(ngModel)]="me.district">\n          <ion-option *ngFor="let distrito of distritos" [value]="distrito.id">{{distrito.distrito}}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Género</ion-label>\n        <ion-select [(ngModel)]="me.gender">\n          <ion-option value="0">No especificado</ion-option>\n          <ion-option value="1">Masculino</ion-option>\n          <ion-option value="2">Femenimo</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Contraseña</ion-label>\n        <ion-label fixed><button (click)="changePassword()" class="buttonPinkOrange" style="top: 7px; left: 124px;" ion-button round>Cambiar</button>\n        </ion-label>\n\n      </ion-item>\n      <div class="buttonWrapper">\n        <button (click)="save()" class="buttonPinkOrange" ion-button round>{{ \'PET.UPDATE_INFO\' | translate }}</button>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/profile/profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* PopoverController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_3__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_date_picker__["a" /* DatePicker */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_9__angular_common__["d" /* DatePipe */]])
], Profile);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalOK; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalOK = (function () {
    function ModalOK(viewCtrl, params, renderer) {
        this.viewCtrl = viewCtrl;
        this.params = params;
        this.renderer = renderer;
        this.msg = params.get('msg');
        this.renderer.addClass(document.body.querySelector("ng-component.app-root"), 'blur');
    }
    ModalOK.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ModalOK.prototype.ngOnDestroy = function () {
        this.renderer.removeClass(document.body.querySelector("ng-component.app-root"), 'blur');
    };
    return ModalOK;
}());
ModalOK = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-modalOK',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/modals/ok/ok.html"*/'<ion-content>\n	<div class="full"></div>\n	<div class="msg">\n	<img src="assets/img/checked.png" style="max-width: 25%; padding-bottom: 30px; display: block; margin: auto;">\n	{{msg}}\n	</div>\n	<button class="buttonPinkOrange small" ion-button round  (click)="close()">Aceptar</button>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/modals/ok/ok.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Renderer2 */]])
], ModalOK);

//# sourceMappingURL=ok.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalERR; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalERR = (function () {
    function ModalERR(viewCtrl, params, renderer) {
        this.viewCtrl = viewCtrl;
        this.params = params;
        this.renderer = renderer;
        this.msg = params.get('msg');
        this.renderer.addClass(document.body.querySelector("ng-component.app-root"), 'blur');
    }
    ModalERR.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ModalERR.prototype.ngOnDestroy = function () {
        this.renderer.removeClass(document.body.querySelector("ng-component.app-root"), 'blur');
    };
    return ModalERR;
}());
ModalERR = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-modalERR',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/modals/err/err.html"*/'<ion-content>\n	<div class="full"></div>\n	<div class="msg">\n	<img src="assets/img/unchecked.png" style="max-width: 25%; padding-bottom: 30px; display: block; margin: auto;">\n	{{msg}}\n	</div>\n	<button class="buttonPinkOrange small" ion-button round  (click)="close()">Aceptar</button>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/modals/err/err.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Renderer2 */]])
], ModalERR);

//# sourceMappingURL=err.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileMedia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfileMedia = (function () {
    function ProfileMedia(navCtrl, navParams, viewCtrl, service, userService, loadingCtrl, camera, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.service = service;
        this.userService = userService;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.storage = storage;
        this.loading = null;
        this.token = '';
        this.userId = null;
        this.storage.get("MP-Profile").then(function (val) {
            _this.userId = val.id;
        });
        this.storage.get("token").then(function (val) {
            _this.token = val;
        });
    }
    ProfileMedia.prototype.ionViewDidLoad = function () {
    };
    ProfileMedia.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ProfileMedia.prototype.takePicture = function () {
        var _this = this;
        this.camera.getPicture({
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 300,
            targetHeight: 300,
            correctOrientation: true
        }).then(function (imageData) {
            _this.processTake(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    ProfileMedia.prototype.openGallery = function () {
        var _this = this;
        var cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 60,
            targetWidth: 320,
            targetHeight: 320,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        this.camera.getPicture(cameraOptions)
            .then(function (file_uri) {
            _this.processTake(file_uri);
        }, function (err) {
            console.log(err);
        });
    };
    ProfileMedia.prototype.dataURItoBlob = function (dataURI) {
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = (dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {
            type: mimeString
        });
    };
    ProfileMedia.prototype.processTake = function (imageData) {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.loading = this.loadingCtrl.create({ content: 'actualizando...' });
        this.loading.present();
        var self0 = this;
        var blob = this.dataURItoBlob(this.base64Image);
        var objURL = window.URL.createObjectURL(blob);
        var image = new Image();
        image.src = objURL;
        window.URL.revokeObjectURL(objURL);
        var url = window.URL.createObjectURL(blob);
        var formData = new FormData();
        formData.append('avatar', blob, 'avatar.jpg');
        formData.append('userId', self0.userId);
        var xhr = new XMLHttpRequest();
        xhr.open("post", __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].apiUrl + "users/avatar");
        xhr.setRequestHeader("Authorization", "Bearer " + self0.token);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var uri = xhr.responseText;
                self0.loading.dismiss();
                if (uri.indexOf('error') > -1) {
                    self0.service.logError({}, 'Error al procesar la solicitud. Inténtelo más tarde.');
                }
                else {
                    self0.service.showOk("Foto actualizada con éxito");
                    self0.userService.getProfile().subscribe(function (result) {
                        result.avatar = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].staticUrl + uri.replace('/public/', '');
                        self0.storage.set("MP-Profile", result);
                        self0.viewCtrl.dismiss(true);
                    }, function (err) {
                        console.log(JSON.stringify(err));
                    });
                }
            }
        };
        xhr.send(formData);
        /*
        this.b64toBlob(this.base64Image,
        function(blob) {
    
            var url = window.URL.createObjectURL(blob);
    
            var formData = new FormData();
            formData.append('avatar', blob, 'avatar.jpg');
            formData.append('userId', self0.userId);
    
            
            var xhr = new XMLHttpRequest();
            xhr.open("post", environment.apiUrl+"users/avatar");
            xhr.setRequestHeader("Authorization", "Bearer "+self0.token);
    
            xhr.onreadystatechange = function () {
    
              if(xhr.readyState === 4 && xhr.status === 200) {
                var uri = xhr.responseText;
                self0.loading.dismiss();
    
                if (uri.indexOf('error') > -1) {
                  self0.service.logError({}, 'Error al procesar la solicitud. Inténtelo más tarde.');
                }
                else {
                  self0.service.showOk("Foto actualizada con éxito");
    
                  self0.userService.getProfile().subscribe(
                    (result)=>{
                      result.avatar = environment.staticUrl+uri.replace('/public/','');
                      self0.storage.set("MP-Profile", result);
                      self0.viewCtrl.dismiss(true);
                    },
                    err => {
                      console.log(JSON.stringify(err));
                    }
                  );
                }
              }
              
    
              
            };
    
            xhr.send(formData);
    
        }, function(error) {
            console.log('error',error);
        });
        */
    };
    ProfileMedia.prototype.b64toBlob = function (b64, onsuccess, onerror) {
        var img = new Image();
        img.onerror = onerror;
        img.onload = function onload() {
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            canvas.toBlob(onsuccess);
        };
        img.src = b64;
    };
    return ProfileMedia;
}());
ProfileMedia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-profile-media',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/popovers/profile-media/profile-media.html"*/'<ion-list radio-group class="popover-page">\n  <div class="close" (click)="close()">\n    <span class="icon-cerrar"></span>\n  </div>\n  <ion-item (click)="takePicture()">\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.TAKE_PICTURE\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-movil"></span>\n    </ion-note>\n  </ion-item>\n  <ion-item (click)="openGallery()">\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.CHOOSE_FROM_ALBUM\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-album"></span>\n    </ion-note>\n  </ion-item>\n  <!--\n  <ion-item>\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.SET_AS_MAIN\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-principal"></span>\n    </ion-note>\n  </ion-item>\n\n  <ion-item>\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.DELETE_PICTURE\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-eliminar"></span>\n    </ion-note>\n  </ion-item>\n  -->\n\n</ion-list>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/popovers/profile-media/profile-media.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_4__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
], ProfileMedia);

//# sourceMappingURL=profile-media.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pata__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChangePasswordPage = (function () {
    function ChangePasswordPage(navCtrl, navParams, userService, service, loadingCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userService = userService;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.viewCtrl = viewCtrl;
        this.user = { currentPass: '', newPass: '', repeatPass: '' };
        //this.userId = this.navParams.get('userId')
    }
    ChangePasswordPage.prototype.ionViewDidLoad = function () {
    };
    ChangePasswordPage.prototype.dismiss = function (data) {
        this.viewCtrl.dismiss(data);
    };
    ChangePasswordPage.prototype.changePassword = function () {
        var _this = this;
        if (this.user.newPass != this.user.repeatPass) {
            this.service.logError(null, "Las contraseñas no coinciden");
            return false;
        }
        var loading = this.loadingCtrl.create({
            content: 'cargando...'
        });
        loading.present();
        console.log(JSON.stringify(this.user));
        this.userService.changePassword(this.user.currentPass, this.user.newPass).subscribe(function (data) {
            loading.dismiss();
            if (data.status) {
                _this.dismiss(true);
                //this.service.showOk();
            }
            else {
                //this.dismiss(false);
                _this.service.logError(null, data.message);
            }
        }, function (error) {
            loading.dismiss();
            console.log('Change.error: ' + JSON.stringify(error));
            _this.service.logError(null, "Error al intentar cambiar la contraseña");
        });
    };
    return ChangePasswordPage;
}());
ChangePasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-change-password',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/change-password/change-password.html"*/'<!--\n  Generated template for the ChangePasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="viewCtrl.dismiss()" class="my-style-for-modal">\n        <ion-icon class="ionicons" name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Cambiar Contraseña</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n\n  <div class="formContainer">\n    <ion-item>\n      <ion-label fixed>Contraseña Actual</ion-label>\n      <ion-input type="password" [(ngModel)]="user.currentPass"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed>Nueva Contraseña</ion-label>\n      <ion-input type="password" [(ngModel)]="user.newPass"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed>Repetir Nueva contraseña</ion-label>\n      <ion-input type="password" [(ngModel)]="user.repeatPass"></ion-input>\n    </ion-item>\n\n\n    <div class="buttonWrapper">\n      <button (click)="changePassword()" class="buttonPinkOrange" ion-button round>Cambiar Contraseña</button>\n    </div>\n  </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/change-password/change-password.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_3__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
], ChangePasswordPage);

//# sourceMappingURL=change-password.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_code_login_code__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupPage = (function () {
    function SignupPage(navCtrl, navParams, service, loadingCtrl, storage, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.auth = auth;
        this.login = null;
        this.perfil = null;
        this.token = '';
        this.login = {
            name: '',
            email: '',
            phone: '',
            password: '',
            repassword: ''
        };
    }
    SignupPage.prototype.gotoCode = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_code_login_code__["a" /* LoginCodePage */], this.login);
    };
    SignupPage.prototype.next = function () {
        var _this = this;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.login.name == "") {
            this.service.logError({}, "Nombre no puede estar vacío");
        }
        else if (this.login.email == "" || !emailRegex.test(this.login.email)) {
            this.service.logError({}, "Email no puede estar vacío o es incorrecto");
        }
        else if (this.login.phone == "" || this.login.phone.length != 9) {
            this.service.logError({}, "Número de teléfono celular es incorrecto, utilice 9 digitos");
        }
        else if (this.login.password == "" || this.login.password.length < 6) {
            this.service.logError({}, "La contraseña debe tener al menos 6 caracteres");
        }
        else if (this.login.password != this.login.repassword) {
            this.service.logError({}, "Las contraseñas no coinciden ");
        }
        else {
            this.loading = this.loadingCtrl.create();
            this.loading.present();
            this.auth.signup(this.login).subscribe(function (data) {
                _this.loading.dismiss();
                if (data.token) {
                    //this.auth.setToken(data.token);
                    //this.storage.set("MP-Profile", data.profile);
                    _this.perfil = data.profile;
                    _this.token = data.token;
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_code_login_code__["a" /* LoginCodePage */], {
                        login: _this.login,
                        perfil: _this.perfil,
                        token: _this.token
                    });
                }
                else {
                    _this.service.logError({}, "Email o Teléfono ya está en uso, si no has recibido el SMS intenta activar tu cuenta mediante el correo electrónico que enviamos.");
                }
            }, function (err) {
                _this.loading.dismiss();
                _this.service.logError({}, "Email o Contraseña inválida");
            });
        }
    };
    SignupPage.prototype.gotoLogin = function () {
        this.navCtrl.pop();
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/signup/signup.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content scrollX="false" scrollY="false" scroll="false" class="noscroll">\n  <div class="background"></div>\n  <div class="overlay"></div>\n  <ion-scroll scrollX="false" scrollY="true">\n    <img class="back-arrow" src="assets/img/back.png" alt="" (click)="gotoLogin()">\n    <img class="logo" src="assets/img/logo_white.svg" alt="">\n    <div class="welcome">{{ \'SIGNUP.WELCOME\' | translate }}</div>\n    <div class="description">{{ \'SIGNUP.ENTER_PHONE_NUMBER_TO_BEGIN\' | translate }}</div>\n    <div class="listWrapper">\n      <ion-list>\n        <ion-item>\n          <ion-input type="text" [(ngModel)]="login.name" placeholder="{{ \'SIGNUP.NAME\' | translate }}"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type="email" [(ngModel)]="login.email" placeholder="{{ \'SIGNUP.EMAIL\' | translate }}"></ion-input>\n        </ion-item>\n        <ion-item class="peruflag">\n          <ion-label> <img src="assets/img/peru.png" style="opacity: 0.8;" /></ion-label>\n          <ion-input type="tel" maxlength="9" [(ngModel)]="login.phone" placeholder="{{ \'SIGNUP.PHONE\' | translate }}"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type="password" [(ngModel)]="login.password" placeholder="{{ \'SIGNUP.PASSWORD\' | translate }}"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type="password" [(ngModel)]="login.repassword" placeholder="{{ \'SIGNUP.REPASSWORD\' | translate }}"></ion-input>\n        </ion-item>\n\n      </ion-list>\n    </div>\n    <div class="buttonWrapper">\n      <button class="buttonPinkOrange" ion-button round full (click)="next()">{{ \'SIGNUP.NEXT\' | translate }}</button>\n    </div>\n    <div class="termsAndConditions">\n      {{ \'SIGNUP.ACCEPT_TERMS_AND_CONDITIONS_1\' | translate }}\n      {{ \'SIGNUP.TERMS_OF_USE\' | translate }}\n      {{ \'SIGNUP.ACCEPT_TERMS_AND_CONDITIONS_2\' | translate }}\n      {{ \'SIGNUP.PRIVACY_POLICY\' | translate }}\n      {{ \'SIGNUP.ACCEPT_TERMS_AND_CONDITIONS_3\' | translate }}\n    </div>\n\n    <div class="buttonWrapper">\n      <button class="buttonPinkOrange small login" ion-button round  (click)="gotoLogin()">{{ \'SIGNUP.TO_LOGIN\' | translate }}</button>\n    </div>\n  </ion-scroll>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/signup/signup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]])
], SignupPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginCodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the LoginCodePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginCodePage = (function () {
    function LoginCodePage(navCtrl, navParams, service, storage, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.storage = storage;
        this.auth = auth;
        this.requestInProgress = false;
        this.code = null;
        this.params = {
            email: '',
            code: ''
        };
        this.login = null;
        this.token = null;
        this.perfil = null;
        //this.params.email = navParams.get('email');
        this.login = navParams.get('login');
        this.perfil = navParams.get('perfil');
        this.token = navParams.get('token');
        this.code = {
            uno: '',
            dos: '',
            tres: '',
            cuatro: ''
        };
    }
    LoginCodePage.prototype.getCode = function () {
        return this.code.uno + this.code.dos + this.code.tres + this.code.cuatro;
    };
    LoginCodePage.prototype.resetInputs = function () {
        this.code.uno = '';
        this.code.dos = '';
        this.code.tres = '';
        this.code.cuatro = '';
    };
    LoginCodePage.prototype.valuechange = function (value, i) {
        i = parseInt(i);
        console.log('valuechange ' + value + ',' + i);
        switch (i) {
            case 1: {
                if (value != "" || value > 0)
                    setTimeout(function () { document.getElementById('e2').querySelector("input").focus(); }, 300);
                break;
            }
            case 2: {
                if (value != "" || value > 0)
                    setTimeout(function () { document.getElementById('e3').querySelector("input").focus(); }, 300);
                break;
            }
            case 3: {
                if (value != "" || value > 0)
                    setTimeout(function () { document.getElementById('e4').querySelector("input").focus(); }, 300);
                break;
            }
            case 4: {
                if (value != "" || value > 0)
                    this.accept();
                break;
            }
            default: {
                break;
            }
        }
    };
    LoginCodePage.prototype.accept = function () {
        var _this = this;
        this.requestInProgress = true;
        this.params.code = this.getCode();
        this.auth.verifyPhone({
            email: this.login.email,
            code: this.getCode()
        }).subscribe(function (result) {
            if (result.status) {
                _this.auth.login({
                    email: _this.login.email,
                    password: _this.login.password
                }).subscribe(function (data) {
                    if (data.token) {
                        _this.auth.setToken(data.token);
                        _this.storage.set("MP-Profile", data.profile);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */], {}, { animate: true, direction: 'forward' });
                    }
                    else {
                        _this.requestInProgress = false;
                        _this.service.logError({}, "Email o Contraseña inválida");
                    }
                }, function (err) {
                    _this.requestInProgress = false;
                    _this.service.logError({}, "Email o Contraseña inválida");
                });
            }
            else {
                _this.requestInProgress = false;
                _this.resetInputs();
                _this.service.logError({}, "Código de verificación invalido", function () {
                    document.getElementById('e1').querySelector("input").focus();
                });
            }
        }, function (err) {
            _this.requestInProgress = false;
            _this.service.logError({}, JSON.stringify(err));
        });
    };
    LoginCodePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    return LoginCodePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])('uno'),
    __metadata("design:type", Object)
], LoginCodePage.prototype, "inp1", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])('dos'),
    __metadata("design:type", Object)
], LoginCodePage.prototype, "inp2", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])('tres'),
    __metadata("design:type", Object)
], LoginCodePage.prototype, "inp3", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])('cuatro'),
    __metadata("design:type", Object)
], LoginCodePage.prototype, "inp4", void 0);
LoginCodePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-login-code',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/login-code/login-code.html"*/'<!--\n  Generated template for the LoginCodePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content scrollX="false" scrollY="false" scroll="false" class="noscroll">\n  <div class="background"></div>\n  <div class="overlay"></div>\n  <ion-scroll scrollX="false" scrollY="true">\n    <img class="logo" src="assets/img/logo_white.svg" alt="">\n    <div class="welcome" *ngIf="!requestInProgress">{{ \'LOGIN_CODE.FEW_MORE_SECONDS\' | translate }}</div>\n    <div class="description" *ngIf="!requestInProgress">{{ \'LOGIN_CODE.ENTER_VERIFICATION_CODE\' | translate }}</div>\n    <div class="welcome" *ngIf="requestInProgress">{{ \'LOGIN_CODE.ONE_STEP\' | translate }}</div>\n    <div class="description" *ngIf="requestInProgress">{{ \'LOGIN_CODE.WE_ARE_VERIFYING\' | translate }}</div>\n    <div class="listWrapper" *ngIf="!requestInProgress">\n      <ion-list>\n        <ion-item>\n          <ion-input type="tel" id="e1" maxlength="1" autofocus [(ngModel)]="code.uno" (ngModelChange)="valuechange($event, 1)" #uno></ion-input>\n          <ion-input type="tel" id="e2" maxlength="1" [(ngModel)]="code.dos" (ngModelChange)="valuechange($event, 2)" #dos></ion-input>\n          <ion-input type="tel" id="e3" maxlength="1" [(ngModel)]="code.tres" (ngModelChange)="valuechange($event, 3)" #tres></ion-input>\n          <ion-input type="tel" id="e4" maxlength="1" [(ngModel)]="code.cuatro" (ngModelChange)="valuechange($event, 4)" #cuatro></ion-input>\n        </ion-item>\n      </ion-list>\n    </div>\n    <div class="buttonWrapper" *ngIf="!requestInProgress">\n      <button class="buttonPinkOrange" ion-button round full (click)="accept()">{{ \'LOGIN_CODE.ACCEPT\' | translate }}</button>\n    </div>\n    <div class="buttonWrapper" *ngIf="!requestInProgress">\n      <button class="buttonWhite" ion-button round full (click)="goBack()">{{ \'LOGIN_CODE.GO_BACK\' | translate }}</button>\n    </div>\n    <div class="termsAndConditions" *ngIf="!requestInProgress">\n      {{ \'LOGIN.ACCEPT_TERMS_AND_CONDITIONS_1\' | translate }}\n      {{ \'LOGIN.TERMS_OF_USE\' | translate }}\n      {{ \'LOGIN.ACCEPT_TERMS_AND_CONDITIONS_2\' | translate }}\n      {{ \'LOGIN.PRIVACY_POLICY\' | translate }}\n      {{ \'LOGIN.ACCEPT_TERMS_AND_CONDITIONS_3\' | translate }}\n    </div>\n    <div class="spinnerWrapper" *ngIf="requestInProgress">\n      <ion-spinner></ion-spinner>\n    </div>\n  </ion-scroll>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/login-code/login-code.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]])
], LoginCodePage);

//# sourceMappingURL=login-code.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnboardingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the OnboardingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var OnboardingPage = (function () {
    function OnboardingPage(navCtrl, navParams, menu, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.storage = storage;
        this.menu.swipeEnable(false, 'leftMenu');
    }
    OnboardingPage.prototype.onIonDrag = function (event) {
        // if (this.slider.getPreviousIndex() >= (this.slider.length() - 1)) {
        //   this.skip();
        // }
    };
    OnboardingPage.prototype.skip = function () {
        this.storage.set("MP-FirstTime", true);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */], {}, { animate: true, direction: 'forward' });
    };
    OnboardingPage.prototype.next = function () {
        if (this.slider.isEnd()) {
            this.skip();
        }
        else {
            this.slider.slideNext();
        }
    };
    return OnboardingPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])('slider'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
], OnboardingPage.prototype, "slider", void 0);
OnboardingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-onboarding',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/onboarding/onboarding.html"*/'<!--\n  Generated template for the OnboardingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content>\n  <ion-slides #slider pager slidesPerView="1" (ionSlideWillChange)="onIonDrag($event)" >\n    <ion-slide id="slide1">\n      <div class="title">{{ \'ONBOARDING.CREATE_ACCOUNT\' | translate }}</div>\n      <div class="card">\n        <img class="step-one" src="./assets/img/onboarding1.png" alt="">\n        <div>{{ \'ONBOARDING.CREATE_ACCOUNT_DESCRIPTION_1\' | translate }}</div>\n        <div class="bold">{{ \'ONBOARDING.CREATE_ACCOUNT_DESCRIPTION_2\' | translate }}</div>\n      </div>\n    </ion-slide>\n    <ion-slide id="slide2">\n      <div class="title">{{ \'ONBOARDING.REGISTER_YOUR_PETS\' | translate }}</div>\n      <div class="card">\n        <img class="step-two" src="./assets/img/onboarding2.png" alt="">\n        <div>\n          {{ \'ONBOARDING.REGISTER_YOUR_PETS_DESCRIPTION_1\' | translate }}\n          <span class="bold">{{ \'ONBOARDING.REGISTER_YOUR_PETS_DESCRIPTION_2\' | translate }}</span>\n          {{ \'ONBOARDING.REGISTER_YOUR_PETS_DESCRIPTION_3\' | translate }}\n        </div>\n      </div>\n    </ion-slide>\n    <ion-slide id="slide3">\n      <div class="title">{{ \'ONBOARDING.ENJOY_APP\' | translate }}</div>\n      <div class="card">\n        <img class="step-three" src="./assets/img/onboarding3.png" alt="">\n        <div>\n          {{ \'ONBOARDING.ENJOY_APP_DESCRIPTION_1\' | translate }}\n          <span class="bold">{{ \'ONBOARDING.ENJOY_APP_DESCRIPTION_2\' | translate }}</span>\n          {{ \'ONBOARDING.ENJOY_APP_DESCRIPTION_3\' | translate }}\n        </div>\n      </div>\n    </ion-slide>\n  </ion-slides>\n  <div class="labels">\n    <div class="labelsWrapper">\n      <div id="skip" (click)="skip()">{{ \'ONBOARDING.SKIP\' | translate }}</div>\n      <div id="next" (click)="next()">{{ \'ONBOARDING.NEXT\' | translate }}</div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/onboarding/onboarding.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], OnboardingPage);

//# sourceMappingURL=onboarding.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Searching; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_pet_pet__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_pet_model__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the MyPetsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Searching = (function () {
    function Searching(navCtrl, navParams, storage, petService, barcodeScanner, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.petService = petService;
        this.barcodeScanner = barcodeScanner;
        this.loadingCtrl = loadingCtrl;
        this.over = false;
        this.tab1 = true;
        this.tab2 = false;
        this.loading = null;
        this.findpets = [];
        this.pets = [];
        this.candidatos = [];
        this.especies = [];
        this.razas = [];
        this.sizes = [];
        this.sexs = [];
        this.search = {
            breed: '',
            type: '',
            age: '',
            sex: '',
            size: ''
        };
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].staticUrl;
        this.mascota_activa = new __WEBPACK_IMPORTED_MODULE_7__models_pet_model__["a" /* Pet */]();
        var load = this.loadingCtrl.create();
        load.present();
        var search = this.petService.searchAvailable(this.search);
        search.subscribe(function (ok) {
            load.dismiss();
            _this.findpets = ok.data;
        }, function (error) {
            load.dismiss();
            console.log('error', error);
            _this.findpets = [];
        });
        this.petService.especie().subscribe(function (ok) {
            _this.especies = ok;
        });
        this.petService.size().subscribe(function (ok) {
            _this.sizes = ok;
        });
        this.petService.sex().subscribe(function (ok) {
            _this.sexs = ok;
        });
        this.storage.get('MP-Profile').then(function (val) {
            _this.petService.getId(val.id).subscribe(function (pets) {
                _this.pets = pets;
            });
        });
    }
    Searching.prototype.refreshRazas = function (e) {
        var _this = this;
        if (e != "") {
            this.petService.raza(e).subscribe(function (ok) {
                _this.razas = ok;
            });
        }
        else {
            this.razas = [];
        }
    };
    Searching.prototype.activarMascota = function (pet) {
        var _this = this;
        this.mascota_activa = pet;
        var load = this.loadingCtrl.create();
        load.present();
        // let find = this.petService.getMyLove();
        /* find.subscribe((ok) => {
           load.dismiss();
         }, (error) => {
           load.dismiss();
         });*/
        var search = this.petService.searchAvailable(this.search);
        search.subscribe(function (ok) {
            load.dismiss();
            _this.findpets = ok.data;
        }, function (error) {
            load.dismiss();
            console.log('error', error);
            _this.findpets = [];
        });
        var recived = this.petService.getMyLoveRecived(this.mascota_activa.id);
        recived.subscribe(function (pets) {
            console.log(JSON.stringify(pets));
            console.log(JSON.stringify(pets.applicant_pet));
            _this.candidatos = pets;
            //load.dismiss();
        }, function (error) {
            //load.dismiss();
        });
    };
    Searching.prototype.Tab1 = function () {
        this.tab1 = true;
        this.tab2 = false;
    };
    Searching.prototype.Tab2 = function () {
        this.tab1 = false;
        this.tab2 = true;
    };
    Searching.prototype.openFiltros = function () {
        this.over = true;
    };
    Searching.prototype.closeFiltros = function () {
        var _this = this;
        this.over = false;
        var loading = this.loadingCtrl.create({
            content: 'filtrando...'
        });
        loading.present();
        var search = this.petService.searchAvailable(this.search);
        search.subscribe(function (ok) {
            _this.findpets = ok.data;
            loading.dismiss();
        }, function (error) {
            loading.dismiss();
            _this.findpets = [];
        });
    };
    Searching.prototype.goToDetail = function (pet) {
        console.log('candidato:' + JSON.stringify(pet));
        console.log('search.myPet:' + JSON.stringify(this.mascota_activa));
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_pet_pet__["a" /* PetPage */], {
            pet: pet,
            myPet: this.mascota_activa,
            petId: this.mascota_activa.id,
            isDetail: false,
            isLove: true,
            isMatch: false,
            isEdit: false
        });
    };
    // 0 solicitud
    // 1 aceptado
    // 2 reject
    Searching.prototype.goToLoveDetail = function (pet) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_pet_pet__["a" /* PetPage */], {
            pet: pet,
            myPet: this.mascota_activa,
            petId: this.mascota_activa.id,
            statusMatch: pet.status,
            isDetail: false,
            isLove: true,
            isMatch: true,
            isEdit: false
        });
    };
    Searching.prototype.getYearOld = function (date) {
        var d = new Date(date);
        var c = new Date();
        d.getFullYear();
        var year = (c.getFullYear() - d.getFullYear());
        return (year >= 0) ? year : 'sin especificar';
    };
    Searching.prototype.getLocation = function (pet) {
        var location = 'sin especificar';
        var district = this.capitalizeFirstLetter(pet.district.toLowerCase());
        var region = this.capitalizeFirstLetter(pet.region.toLowerCase());
        if (district.length > 0 && region.length > 0) {
            location = district + ', ' + region;
        }
        else if (district.length > 0 || region.length > 0) {
            location = district + region;
        }
        return location;
    };
    Searching.prototype.capitalizeFirstLetter = function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    return Searching;
}());
Searching = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-searching',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/searching/searching.html"*/'<!--\n  Generated template for the MyPetsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <span class="icon-menu"></span>\n    </button>\n    <ion-title>{{ \'APP_MENU.LOOKING_FOR_PARTNERS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="over" *ngIf="over">\n    <ion-grid>\n      <ion-row>\n      <ion-col col-9><h1>Filtros de búsqueda</h1> </ion-col>\n      <ion-col col-3 text-center><img (click)="closeFiltros();" src="assets/img/close.png" style="max-width: 40px;" /></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Especie</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.type" (ionChange)="refreshRazas($event)">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let species of especies" [value]="species.id">{{species.species}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Raza</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.breed">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let breed of razas" [value]="breed.id">{{breed.breed}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Edad</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.age">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option value="0-1">Menos de un año</ion-option>\n            <ion-option value="1-3">entre 1 y 3 años</ion-option>\n            <ion-option value="3-5">entre 3 y 5 años</ion-option>\n            <ion-option value="5-7">entre 5 y 7 años</ion-option>\n            <ion-option value="7-10">entre 7 y 10 años</ion-option>\n            <ion-option value="10-1000">más de 10 años</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Sexo</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.sex">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let sex of sexs" [value]="sex.id">{{sex.value}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Tamaño</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.size">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let size of sizes" [value]="size.id">{{size.value}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <div class="tabs t">\n    <button (click)="Tab1()" [ngClass]="tab1 ? \'activo\' : \'\'">Buscando pareja</button>\n    <button (click)="Tab2()" [ngClass]="tab2 ? \'activo\' : \'\'">Mis candidatos</button>\n  </div>\n\n  <div [hidden]="!tab1">\n    <!-- start Banner -->\n    <ins data-revive-zoneid="9" data-revive-id="9d276fa041e1067786e857fe85ab98f9"></ins>\n    <!-- end Banner -->\n\n    <div class="slidesWrapper">\n      <ion-slides pager slidesPerView="4.5">\n        <ion-slide *ngFor="let pet of pets">\n          <pet-preview [pet]="pet" (click)="activarMascota(pet)" [preventDetail]="true" [isDetail]="false" [isMe]="true" [add]="false"></pet-preview>\n        </ion-slide>\n      </ion-slides>\n    </div>\n\n    <div class="t" *ngIf="pets.length == 0" style="text-align:center;">\n      No tienes ninguna mascota, conoce candidatos agregando tu primera mascota en <b>Mis Mascotas</b>\n    </div>\n\n    <div class="f0" *ngIf="mascota_activa.id">\n      <ion-grid class="t">\n        <ion-row>\n        <ion-col col-9><h1>Actualmente buscando pareja para {{mascota_activa.name}}</h1> </ion-col>\n        <ion-col col-3 text-center><img (click)="openFiltros();"  src="assets/img/filter.png" style="max-width: 40px;" /></ion-col>\n        </ion-row>\n      </ion-grid>\n\n      <div text-center *ngIf="findpets.length == 0"><strong>No hay mascotas encontradas</strong></div>\n\n      <ion-row class="t" responsive-sm wrap>\n        <ion-col col-6 *ngFor="let pet of findpets">\n         <div class="rondme" (click)="goToDetail(pet)">\n          <div class="foto" [style.background-image]="\'url(\' + (pet.avatar != \'\' ? staticUrl+pet.avatar : \'assets/img/pet.png\' ) + \')\'" style="background-size: cover;">\n          </div>\n          <div class="name">{{pet.name}}</div>\n          <div class="sub">Especie: {{pet.type_name}}</div>\n          <div class="sub">Raza: {{pet.breed_name}}</div>\n          <div class="sub">Sexo: {{pet.sex_name}}</div>\n          <div class="sub">Tamaño: {{pet.size_name}}</div>\n          <div class="sub">Edad: {{getYearOld(pet.birthday)}}</div>\n           <div class="sub location"><ion-icon class="ion-icon" name="pin"></ion-icon>{{getLocation(pet)}}</div>\n         </div>\n        </ion-col>\n      </ion-row>\n    </div>\n\n  </div>\n  <div [hidden]="!tab2">\n    <!-- start Banner -->\n    <ins data-revive-zoneid="9" data-revive-id="9d276fa041e1067786e857fe85ab98f9"></ins>\n    <!-- end Banner -->\n    <div class="slidesWrapper">\n      <ion-slides pager slidesPerView="4.5">\n        <ion-slide *ngFor="let pet of pets">\n          <pet-preview [pet]="pet" (click)="activarMascota(pet)" [preventDetail]="true" [isDetail]="false" [isMe]="true" [add]="false"></pet-preview>\n        </ion-slide>\n      </ion-slides>\n    </div>\n\n    <div class="t" *ngIf="pets.length == 0" style="text-align:center;">\n      No tienes ninguna mascota, conoce candidatos agregando tu primera mascota en <b>Mis Mascotas</b>\n    </div>\n\n    <div class="f0" *ngIf="mascota_activa.id">\n      <ion-grid class="t" *ngIf="candidatos.length > 0">\n        <ion-row>\n          <ion-col col-12><h1>Estos son los candidatos para {{ mascota_activa.name }}</h1> </ion-col>\n\n          <ion-col col-6 *ngFor="let pet of candidatos">\n            <div class="rondme" (click)="goToLoveDetail(pet)">\n              <div class="foto" [style.background-image]="\'url(\' + (pet.avatar && pet.avatar != \'\' ? staticUrl+pet.avatar : \'assets/img/default/mascota.png\' ) + \')\'" style="background-size: cover;">\n              </div>\n              <div class="name">{{pet.name}}</div>\n              <div class="sub">Especie: {{pet.type_name}}</div>\n              <div class="sub">Raza: {{pet.breed_name}}</div>\n              <div class="sub">Sexo: {{pet.sex_name}}</div>\n              <div class="sub">Tamaño: {{pet.size_name}}</div>\n              <div class="sub">Edad: {{getYearOld(pet.birthday)}}</div>\n            </div>\n          </ion-col>\n\n        </ion-row>\n      </ion-grid>\n\n      <ion-grid class="t" *ngIf="candidatos.length == 0">\n        <ion-row>\n          <ion-col col-12><h1>No hay candidatos activos para {{ mascota_activa.name }}</h1> </ion-col>\n        </ion-row>\n      </ion-grid>\n\n    </div>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/searching/searching.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__services_pet_service__["a" /* PetService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
], Searching);

//# sourceMappingURL=searching.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetMediaOptionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PetMediaOptionsPage = (function () {
    function PetMediaOptionsPage(navCtrl, navParams, viewCtrl, service, userService, petService, loadingCtrl, camera, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.service = service;
        this.userService = userService;
        this.petService = petService;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.storage = storage;
        this.imagenIndex = 0;
        this.loading = null;
        this.token = '';
        this.petId = null;
        this.additionalImages = null;
        this.canDelete = false;
        this.canMark = false;
        this.imagenIndex = this.navParams.get("imagenIndex");
        this.additionalImages = this.navParams.get("additionalImages");
        this.petId = this.navParams.get("id");
        if (this.additionalImages != "") {
            this.canMark = true;
            this.canDelete = true;
        }
        this.storage.get("token").then(function (val) {
            _this.token = val;
        });
    }
    PetMediaOptionsPage.prototype.ionViewDidLoad = function () {
    };
    PetMediaOptionsPage.prototype.close = function () {
        this.viewCtrl.dismiss({ reload: false });
    };
    PetMediaOptionsPage.prototype.takePicture = function () {
        var _this = this;
        this.camera.getPicture({
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            correctOrientation: true
        }).then(function (imageData) {
            _this.processTake(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    PetMediaOptionsPage.prototype.openGallery = function () {
        var _this = this;
        var cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 70,
            targetWidth: 620,
            targetHeight: 620,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        this.camera.getPicture(cameraOptions)
            .then(function (file_uri) { return _this.processTake(file_uri); }, function (err) { return console.log(err); });
    };
    PetMediaOptionsPage.prototype.dataURItoBlob = function (dataURI) {
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = (dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {
            type: mimeString
        });
    };
    PetMediaOptionsPage.prototype.processTake = function (imageData) {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.loading = this.loadingCtrl.create({ content: 'subiendo foto...' });
        this.loading.present();
        var self0 = this;
        var blob = this.dataURItoBlob(this.base64Image);
        var objURL = window.URL.createObjectURL(blob);
        var image = new Image();
        image.src = objURL;
        window.URL.revokeObjectURL(objURL);
        var url = window.URL.createObjectURL(blob);
        var formData = new FormData();
        formData.append('avatar', blob, 'avatar.jpg');
        var xhr = new XMLHttpRequest();
        xhr.open("post", __WEBPACK_IMPORTED_MODULE_7__environments_environment__["a" /* environment */].apiUrl + "pets/avatar/" + self0.petId);
        xhr.setRequestHeader("Authorization", "Bearer " + self0.token);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var uri = xhr.responseText;
                self0.loading.dismiss();
                if (uri.indexOf('error') > -1) {
                    self0.service.logError({}, 'Error al procesar la solicitud. Inténtelo más tarde.');
                }
                else {
                    self0.service.showOk("Foto agregada con éxito");
                    self0.viewCtrl.dismiss({ reload: true });
                }
            }
        };
        xhr.send(formData);
        /*
        this.b64toBlob(this.base64Image,
        function(blob) {
            var url = window.URL.createObjectURL(blob);
            // do something with url
            var formData = new FormData();
            formData.append('avatar', blob, 'avatar.jpg');
    
            var xhr = new XMLHttpRequest();
            xhr.open("post", environment.apiUrl+"pets/avatar/"+self0.petId);
            xhr.setRequestHeader("Authorization", "Bearer "+self0.token);
    
            xhr.onreadystatechange = function () {
    
              if(xhr.readyState === 4 && xhr.status === 200) {
                var uri = xhr.responseText;
                self0.loading.dismiss();
    
                if (uri.indexOf('error') > -1) {
                  self0.service.logError({}, 'Error al procesar la solicitud. Inténtelo más tarde.');
                }
                else {
                  self0.service.showOk("Foto agregada con éxito");
                  self0.viewCtrl.dismiss({reload:true});
                }
              }
    
    
    
            };
    
            xhr.send(formData);
    
        }, function(error) {
            console.log('error',error);
        });
    
        */
    };
    PetMediaOptionsPage.prototype.b64toBlob = function (b64, onsuccess, onerror) {
        var img = new Image();
        img.onerror = onerror;
        img.onload = function onload() {
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            canvas.toBlob(onsuccess);
        };
        img.src = b64;
    };
    PetMediaOptionsPage.prototype.markMain = function (index) {
        var _this = this;
        this.loading = this.loadingCtrl.create({ content: 'guardando...' });
        this.loading.present();
        var ps = this.petService.mainImage(this.petId, index);
        ps.subscribe(function (ok) {
            _this.loading.dismiss();
            _this.service.showOk("Foto marcada como principal");
            _this.viewCtrl.dismiss({ reload: false });
        }, function (error) {
            _this.loading.dismiss();
            _this.service.logError({}, 'Error al procesar la solicitud. Inténtelo más tarde.');
            _this.viewCtrl.dismiss({ reload: false });
        });
    };
    PetMediaOptionsPage.prototype.delete = function (index) {
        var _this = this;
        this.loading = this.loadingCtrl.create({ content: 'guardando...' });
        this.loading.present();
        var ps = this.petService.deleteImage(this.petId, index);
        ps.subscribe(function (ok) {
            _this.loading.dismiss();
            _this.service.showOk("Foto eliminada");
            _this.viewCtrl.dismiss({ reload: true });
        }, function (error) {
            _this.loading.dismiss();
            _this.service.logError({}, 'Error al procesar la solicitud. Inténtelo más tarde.');
            _this.viewCtrl.dismiss({ reload: false });
        });
    };
    return PetMediaOptionsPage;
}());
PetMediaOptionsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-pet-media-options',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/popovers/pet-media-options/pet-media-options.html"*/'<ion-list radio-group class="popover-page" style="border-radius: 10px">\n  <div class="close" (click)="close()">\n    <span class="icon-cerrar"></span>\n  </div>\n  <ion-item (click)="takePicture()" style="border-radius: 10px 10px 0px 0px;">\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.TAKE_PICTURE\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-movil"></span>\n    </ion-note>\n  </ion-item>\n  <ion-item (click)="openGallery()">\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.CHOOSE_FROM_ALBUM\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-album"></span>\n    </ion-note>\n  </ion-item>\n\n  <ion-item *ngIf="canMark" (click)="markMain(imagenIndex)">\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.SET_AS_MAIN\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-principal"></span>\n    </ion-note>\n  </ion-item>\n\n  <ion-item *ngIf="canDelete" (click)="delete(imagenIndex)" style="border-radius: 10px;">\n    <ion-label>{{ \'POPOVER_PET_MEDIA_OPTIONS.DELETE_PICTURE\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-eliminar"></span>\n    </ion-note>\n  </ion-item>\n\n\n</ion-list>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/popovers/pet-media-options/pet-media-options.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_4__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_5__services_pet_service__["a" /* PetService */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
], PetMediaOptionsPage);

//# sourceMappingURL=pet-media-options.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetStatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_my_pets_my_pets__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PetStatePage = (function () {
    function PetStatePage(navCtrl, navParams, viewCtrl, petService, loadingCtrl, service, geolocation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.petService = petService;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.geolocation = geolocation;
        this.petId = 0;
        this.petMissing = false;
        this.petLoving = false;
        this.lat = 0;
        this.lng = 0;
        this.petId = this.navParams.get("id");
        this.petMissing = (this.navParams.get("petMissing").toString() == "0" ? false : true);
        this.petLoving = (this.navParams.get("petLoving").toString() == "0" ? false : true);
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.lat = resp.coords.latitude;
            _this.lng = resp.coords.longitude;
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    }
    PetStatePage.prototype.ionViewDidLoad = function () {
    };
    PetStatePage.prototype.setEstado = function (id, state) {
        var _this = this;
        this.close();
        console.log('set state: ' + id + ' - ' + state);
        var obj = {
            lat: this.lat,
            lng: this.lng
        };
        var setState = this.petService.setState(id, state, obj);
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        setState.subscribe(function (ok) {
            loading.dismiss();
            if (state == 'lost') {
                _this.service.showOk("Has reportado a tu mascota como perdida. Recibirás una notificación si alguiente tiene información o si tu mascota ha sido encontrada.");
                _this.petMissing = true;
                /*this.viewCtrl.dismiss({
                  petMissing: this.petMissing,
                  petLoving: this.petLoving
                });*/
            }
            else if (state == 'searching') {
                _this.service.showOk("Desde ahora, tu mascota está disponible en la sección buscando pareja");
                _this.petLoving = true;
                /*this.viewCtrl.dismiss({
                  petMissing: this.petMissing,
                  petLoving: this.petLoving
                });*/
            }
            else if (state == 'nosearching') {
                _this.service.showOk("Desde ahora, tu mascota dejó de estár disponible en la sección buscando pareja");
                _this.petLoving = false;
                _this.viewCtrl.dismiss({
                    petMissing: _this.petMissing,
                    petLoving: _this.petLoving
                });
            }
            else if (state == 'found') {
                _this.service.showOk("Has reportado a tu mascota como encontrada");
                _this.petMissing = false;
                /*this.viewCtrl.dismiss({
                  petMissing: this.petMissing,
                  petLoving: this.petLoving
                });*/
            }
            else if (state == 'remove') {
                _this.service.showOk();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_my_pets_my_pets__["a" /* MyPetsPage */]);
            }
        }, function (error) {
            loading.dismiss();
            _this.service.logError(null, "No fue posible procesar la solicitud. intente nuevamente");
        });
    };
    PetStatePage.prototype.close = function () {
        this.viewCtrl.dismiss({
            petMissing: this.petMissing,
            petLoving: this.petLoving
        });
    };
    return PetStatePage;
}());
PetStatePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-pet-state',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/popovers/pet-state/pet-state.html"*/'<ion-list radio-group class="popover-page" style="border-radius:10px;">\n  <div class="close" (click)="close()">\n    <span class="icon-cerrar"></span>\n  </div>\n  <ion-item *ngIf="!petMissing" (click)="setEstado(petId, \'lost\');" style="border-radius:10px;">\n    <ion-label>{{ \'POPOVER_PET_STATE.LOST_PET\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-lupa"></span>\n    </ion-note>\n  </ion-item>\n  <ion-item *ngIf="petMissing" (click)="setEstado(petId, \'found\');">\n    <ion-label>{{ \'POPOVER_PET_STATE.PET_FOUND\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-lupa"></span>\n    </ion-note>\n  </ion-item>\n  <ion-item *ngIf="!petLoving" (click)="setEstado(petId, \'searching\');">\n    <ion-label>{{ \'POPOVER_PET_STATE.I_AM_LOOKING_FOR_PARTNER\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-corazon"></span>\n    </ion-note>\n  </ion-item>\n  <ion-item *ngIf="petLoving" (click)="setEstado(petId, \'nosearching\');">\n    <ion-label>No estoy buscando pareja</ion-label>\n    <ion-note item-right>\n      <span class="icon-corazon"></span>\n    </ion-note>\n  </ion-item>\n  <ion-item (click)="setEstado(petId, \'remove\');" style="border-radius:10px;">\n    <ion-label>{{ \'POPOVER_PET_STATE.DELETE_PET\' | translate }}</ion-label>\n    <ion-note item-right>\n      <span class="icon-eliminar"></span>\n    </ion-note>\n  </ion-item>\n</ion-list>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/popovers/pet-state/pet-state.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_pet_service__["a" /* PetService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__pata__["a" /* Pata */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */]])
], PetStatePage);

//# sourceMappingURL=pet-state.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Lost; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_pet_pet__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_user_service__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var Lost = (function () {
    function Lost(navCtrl, service, navParams, petService, userService, barcodeScanner, loadingCtrl, storage, platform, geolocation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.service = service;
        this.navParams = navParams;
        this.petService = petService;
        this.userService = userService;
        this.barcodeScanner = barcodeScanner;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.platform = platform;
        this.geolocation = geolocation;
        this.user = null;
        this.loading = null;
        this.findpets = [];
        this.mylostpets = [];
        this.tab1 = true;
        this.tab2 = false;
        this.staticUrl = '';
        this.over = false;
        this.search = {
            breed: '',
            type: '',
            age: '',
            sex: '',
            size: ''
        };
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].staticUrl;
        //this.filtro();
        this.userService.getProfile().subscribe(function (data) {
            _this.user = data;
        }, function (error) { });
    }
    Lost.prototype.Tab1 = function () {
        this.tab1 = true;
        this.tab2 = false;
    };
    Lost.prototype.Tab2 = function () {
        this.tab1 = false;
        this.tab2 = true;
    };
    Lost.prototype.filtro = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create();
        this.loading.present();
        var lost = this.petService.searchLost(this.search);
        lost.subscribe(function (ok) {
            _this.findpets = ok.data;
            _this.storage.get('MP-Profile').then(function (val) {
                _this.petService.getId(val.id).subscribe(function (pets) {
                    _this.mylostpets = [];
                    for (var i = 0; i < pets.length; i++) {
                        if (pets[i].missing == 1) {
                            _this.mylostpets.push(pets[i]);
                        }
                    }
                    _this.loading.dismiss();
                });
            });
        }, function (error) {
            _this.loading.dismiss();
            console.log('error', error);
            _this.findpets = [];
        });
    };
    Lost.prototype.ionViewDidEnter = function () {
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].staticUrl;
        this.filtro();
    };
    Lost.prototype.scanear = function () {
        var _this = this;
        if (this.platform.is('cordova')) {
            this.barcodeScanner.scan().then(function (barcodeData) {
                if (barcodeData.text.indexOf('mimaskot.pe') > -1) {
                    var data = barcodeData.text.split('/');
                    var code = data[data.length - 1];
                    _this.loading = _this.loadingCtrl.create();
                    _this.loading.present();
                    _this.geolocation.getCurrentPosition().then(function (resp) {
                        var data = {
                            "code": code.trim(),
                            "lat_seen": resp.coords.latitude,
                            "lng_seen": resp.coords.longitude,
                            "user_id": _this.user.id
                        };
                        _this.petSeeIn(data);
                    }).catch(function (error) {
                        console.log('Error getting location', error);
                        /** Enviar notificación son ubicación */
                        var data = {
                            "code": code.trim(),
                            "lat_seen": '',
                            "lng_seen": '',
                            "user_id": _this.user.id
                        };
                        _this.petSeeIn(data);
                        //
                    });
                }
                else if (barcodeData.text.length > 0) {
                    _this.service.logError({}, 'QR escaneado no corresponde a MiMaskot');
                }
                else {
                    // did not take the qr code
                }
            }, function (err) {
                alert('Error: ' + JSON.stringify(err));
                _this.service.logError(null, "Servicio temporalmente no disponible");
            });
        }
        else {
        }
    };
    /** Enviar notificación cuando haya visto una mascota perdida */
    Lost.prototype.petSeeIn = function (data) {
        var _this = this;
        console.log("petSeeIn: " + JSON.stringify(data));
        var petcheck = this.petService.notifyFoundPetFromQr(data);
        petcheck.subscribe(function (ok) {
            _this.goToPetFound(ok);
        }, function (err) {
            _this.loading.dismiss();
            _this.service.logError({}, 'No fue posible validar el QR');
        });
    };
    Lost.prototype.goToPetFound = function (ok) {
        if (ok.hasOwnProperty("status")) {
            this.loading.dismiss();
            this.service.logError({}, 'Código QR no encontrado como una mascota perdida');
        }
        else {
            this.loading.dismiss();
            this.service.showOk('Enhorabuena, haz encontrado una mascota que está extraviada. Hemos enviado una notificación a su dueño, si lo deseas puedes contactarlo tú también.');
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_pet_pet__["a" /* PetPage */], {
                pet: ok,
                isDetail: 0,
                isLove: false,
                isLost: true,
                isEdit: false,
                isFound: true
            });
        }
    };
    Lost.prototype.goToDetail = function (pet, my) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_pet_pet__["a" /* PetPage */], {
            pet: pet,
            isDetail: my,
            isLove: false,
            isLost: true,
            isEdit: false,
            isFound: false,
            lastView: my
        });
    };
    Lost.prototype.openFiltros = function () {
        this.over = true;
    };
    Lost.prototype.closeFiltros = function () {
        this.over = false;
        this.filtro();
    };
    return Lost;
}());
Lost = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-lost',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/lost/lost.html"*/'<!--\n  Generated template for the MyPetsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <span class="icon-menu"></span>\n    </button>\n    <ion-title>{{ \'APP_MENU.LOST_PETS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="over" *ngIf="over">\n    <ion-grid>\n      <ion-row>\n      <ion-col col-9><h1>Filtros de búsqueda</h1> </ion-col>\n      <ion-col col-3 text-center><img (click)="closeFiltros();" src="assets/img/close.png" style="max-width: 40px;" /></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Especie</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.type" (ionChange)="refreshRazas($event)">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let species of especies" [value]="species.id">{{species.species}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Raza</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.breed">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let breed of razas" [value]="breed.id">{{breed.breed}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Edad</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.age">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option value="0-1">Menos de un año</ion-option>\n            <ion-option value="1-3">entre 1 y 3 años</ion-option>\n            <ion-option value="3-5">entre 3 y 5 años</ion-option>\n            <ion-option value="5-7">entre 5 y 7 años</ion-option>\n            <ion-option value="7-10">entre 7 y 10 años</ion-option>\n            <ion-option value="10-1000">más de 10 años</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Sexo</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.sex">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let sex of sexs" [value]="sex.id">{{sex.value}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>Tamaño</ion-col>\n        <ion-col col-6>\n          <ion-select [(ngModel)]="search.size">\n            <ion-option value="">Cualquiera</ion-option>\n            <ion-option *ngFor="let size of sizes" [value]="size.id">{{size.value}}</ion-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class="tabs">\n    <button (click)="Tab1()" [ngClass]="tab1 ? \'activo\' : \'\'">Mascotas perdidas</button>\n    <button (click)="Tab2()" [ngClass]="tab2 ? \'activo\' : \'\'">Encontré una mascota</button>\n  </div>\n\n  <div *ngIf="tab1">\n    <div class="f0">\n      <!-- start Banner -->\n      <ins data-revive-zoneid="9" data-revive-id="9d276fa041e1067786e857fe85ab98f9"></ins>\n      <!-- end Banner -->\n      <div class="t">\n        <h1 *ngIf="mylostpets.length > 0"><br>MIS MASCOTAS PERDIDAS<br></h1>\n        <ion-row responsive-sm wrap>\n          <ion-col col-6 *ngFor="let pet of mylostpets">\n           <div class="rondme" (click)="goToDetail(pet,true)">\n            <div class="foto" [style.background-image]="\'url(\' + (pet.avatar != \'\' ? staticUrl+pet.avatar : \'assets/img/default/pet.png\' ) + \')\'" style="background-size: cover;">\n            </div>\n            <div class="name">{{pet.name}}</div>\n            <div class="sub">Especie: {{pet.type_name}}</div>\n            <div class="sub">Raza: {{pet.breed_name}}</div>\n            <div class="sub">Sexo: {{pet.sex_name}}</div>\n            <div class="sub">Tamaño: {{pet.size_name}}</div>\n            <div class="sub">Edad: {{pet.name}}</div>\n           </div>\n          </ion-col>\n        </ion-row>\n\n      <ion-grid class="t">\n        <ion-row>\n        <ion-col col-10><h1>MASCOTAS REPORTADAS COMO PERDIDAS</h1> </ion-col>\n        <ion-col col-2 text-center><img (click)="openFiltros();"  src="assets/img/filter.png" style="max-width: 40px; margin: 10px 0px;q" /></ion-col>\n        </ion-row>\n      </ion-grid>\n\n\n        <div text-center *ngIf="findpets.length == 0"><strong>No hay reportes</strong></div>\n\n        <ion-row responsive-sm wrap>\n          <ion-col col-6 *ngFor="let pet of findpets">\n           <div class="rondme" (click)="goToDetail(pet,false)">{{pet.avatar}}\n            <div class="foto" [style.background-image]="\'url(\' + (pet.avatar != \'\' ? staticUrl+pet.avatar : \'assets/img/default/pet.png\' ) + \')\'" style="background-size: cover;">\n            </div>\n            <div class="name">{{pet.name}}</div>\n            <div class="sub">Especie: {{pet.type_name}}</div>\n            <div class="sub">Raza: {{pet.breed_name}}</div>\n            <div class="sub">Sexo: {{pet.sex_name}}</div>\n            <div class="sub">Tamaño: {{pet.size_name}}</div>\n            <div class="sub">Edad: {{pet.name}}</div>\n           </div>\n          </ion-col>\n        </ion-row>\n\n      </div>\n    </div>\n  </div>\n  <div *ngIf="tab2">\n    <div class="f0">\n      <!-- start Banner -->\n      <ins data-revive-zoneid="9" data-revive-id="9d276fa041e1067786e857fe85ab98f9"></ins>\n      <!-- end Banner -->\n      <div class="t">\n        <h1><br>ENCONTRÉ UNA MASCOTA PERDIDA<br></h1>\n        <p>Si encontraste una mascota perdida, escanea su código adherido a su collar para avisar a su dueño.</p>\n        <div class="buttonWrapper">\n          <button class="buttonPinkOrange" (click)="scanear()" ion-button round>{{ \'BUTTONS.SCAN\' | translate }}</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/lost/lost.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__services_pet_service__["a" /* PetService */],
        __WEBPACK_IMPORTED_MODULE_9__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__["a" /* Geolocation */]])
], Lost);

//# sourceMappingURL=lost.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Doctor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agm_core__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_clinic_clinic__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_doctor_service__ = __webpack_require__(298);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Doctor = (function () {
    function Doctor(navCtrl, navParams, doctorService, geolocation, gMaps, toastCtrl, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.doctorService = doctorService;
        this.geolocation = geolocation;
        this.gMaps = gMaps;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.lat = -12.1215155;
        this.lng = -77.0376026;
        this.styles = [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#6195a0" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [{ "color": "#e7e5e3" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#e6f3d6" }, { "visibility": "on" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": "-100" }, { "lightness": 45 }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#e3d6c7" }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "elementType": "labels.text", "stylers": [{ "color": "#4e4e4e" }] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{ "color": "#f4f4f4" }] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "color": "#787878" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#eaf6f8" }, { "visibility": "on" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#eaf6f8" }] }];
        this.list = [];
        this.buscando = [];
        this.buscar = '';
        this.secureSearch = '';
        this.activo = null;
        this.verDetalle = false;
        this.lat = -12.0732813;
        this.lng = -77.0205312;
        this.doctorService.getAll().subscribe(function (data) {
            _this.list = data.data;
            console.log(JSON.stringify(data));
            console.log(JSON.stringify(data.data));
            console.log(_this.list.length);
            for (var i = 0; i < _this.list.length; i++) {
                _this.list[i].lat = parseFloat(_this.list[i].lat);
                _this.list[i].lng = parseFloat(_this.list[i].lng);
                _this.list[i].icon = "assets/img/marker.png";
            }
        });
    }
    Doctor.prototype.clearBuscar = function () {
        this.buscando = [];
        this.lat = -12.0732813;
        this.lng = -77.0205312;
        this.buscar = '';
    };
    Doctor.prototype.gotome = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.lat = resp.coords.latitude;
            _this.lng = resp.coords.longitude;
            //const position = new google.maps.LatLng(this.lat, this.lng);
            //this.map.panTo(position);
        }).catch(function (error) {
            var toast = _this.toastCtrl.create({
                message: 'No podemos activar tu GPS',
                duration: 1500,
                position: 'bottom',
                showCloseButton: true,
                closeButtonText: 'OK'
            });
            toast.present();
        });
    };
    Doctor.prototype.search = function (event) {
        console.log(event.target.value);
    };
    Doctor.prototype.mapclick = function (obj) {
        this.activo = obj;
        this.verDetalle = true;
    };
    Doctor.prototype.gotoPlace = function (o) {
        var _this = this;
        this.buscando = [];
        this.buscar = o.description;
        console.log(o);
        this.http.get("http://www.enlanube.cl/apis/mipata.php?site=" + o.reference).subscribe(function (data) {
            _this.lat = data.result.geometry.location.lat;
            _this.lng = data.result.geometry.location.lng;
        });
    };
    Doctor.prototype.close = function () {
        this.verDetalle = false;
    };
    Doctor.prototype.moveTo = function (buscar) {
        var _this = this;
        this.secureSearch = buscar; // not used
        if (buscar.length > 2) {
            this.http.get("http://www.enlanube.cl/apis/mipata.php?buscar=" + buscar).subscribe(function (data) {
                if (data.predictions) {
                    _this.buscando = data.predictions;
                }
            });
        }
        else {
            this.buscando = [];
        }
        // console.log('MOVETO:' + buscar);
        // TODO: como no se conoce reply de API, se debe iterar los nombres, y mover el centro del mapa a esa coordenada.
        // mover con:
        /*setTimeout(() => {
    
            if (this.secureSearch == this.buscar) {
                this.secureSearch = '';
    
                let toast = this.toastCtrl.create({
                    message: 'No hay resultados para '+buscar,
                    duration: 1500,
                    position: 'bottom',
                    showCloseButton: true,
                    closeButtonText: 'OK'
                });
    
                toast.onDidDismiss(() => {
    
                });
    
                toast.present();
            }
    
        },1500);*/
    };
    Doctor.prototype.vermas = function (obj) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_clinic_clinic__["a" /* Clinic */], {
            clinic: obj
        });
    };
    Doctor.prototype.loadAPIWrapper = function (map) {
        this.map = map;
    };
    return Doctor;
}());
Doctor = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-doctor',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/doctor/doctor.html"*/'<!--\n  Generated template for the MyPetsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <span class="icon-menu"></span>\n    </button>\n    <ion-title>{{ \'APP_MENU.VETERINARY_CLINICS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card class="overcard">\n    <ion-label> <ion-icon (click)="clearBuscar()" name="close"></ion-icon></ion-label>\n    <ion-item>\n      <ion-input (keyup)=moveTo(buscar) [(ngModel)]="buscar" type="text" placeholder="Buscar..."></ion-input>\n    </ion-item>\n  </ion-card>\n  \n  <div class="search_result" *ngIf="buscando.length > 0">\n    <ion-scroll>\n    <ul>\n      <li *ngFor="let b of buscando" (click)="gotoPlace(b)"><img src="assets/img/chevron.png" />&nbsp;&nbsp;{{b.description}}</li>\n    </ul>\n    </ion-scroll>\n  </div>\n\n  <div class="findme" (click)="gotome()">\n    <img src="assets/img/findme.png" />\n  </div>\n\n  <agm-map [latitude]="lat" [longitude]="lng" [styles]="styles" [zoom]="12" [zoomControl]="false" [streetViewControl]="false" (onMapLoad)=\'loadAPIWrapper($event)\'>\n    <agm-marker *ngFor="let pos of list" (markerClick)="mapclick(pos)" [iconUrl]="pos.icon" [latitude]="pos.lat" [longitude]="pos.lng">\n    </agm-marker>\n  </agm-map>\n  <!--\n  <agm-map *ngFor="let pos of list" [latitude]="lat" [longitude]="lng" [styles]="styles" [zoom]="15" [zoomControl]="false" [streetViewControl]="false" (onMapLoad)=\'loadAPIWrapper($event)\'>\n    <agm-marker (markerClick)="mapclick(pos)" [iconUrl]="pos.icon" [latitude]="pos.lat" [longitude]="pos.lng">\n    </agm-marker>\n  </agm-map>\n  -->\n  <div class="info_vet" *ngIf="verDetalle">\n\n    <div class="buttonWrapper">\n      <button class="buttonPinkOrange" (click)="vermas(activo)" ion-button round>Más información</button>\n    </div>\n\n    <div class="close" (click)="close()">\n      <span class="icon-cerrar"></span>\n    </div>\n\n    <div class="box">\n      <b>{{activo.name}}</b>\n      <br />\n      <small>{{activo.description}}</small>\n    </div>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/doctor/doctor.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__services_doctor_service__["a" /* DoctorService */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
        __WEBPACK_IMPORTED_MODULE_2__agm_core__["b" /* GoogleMapsAPIWrapper */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */]])
], Doctor);

//# sourceMappingURL=doctor.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Clinic; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Clinic = (function () {
    function Clinic(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.loaded = false;
        this.isBlurred = false;
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].staticUrl;
        this.clinic = navParams.get('clinic');
        this.loaded = true;
    }
    Clinic.prototype.call = function () {
        var newWindow = window.open('tel:' + this.clinic.phone);
    };
    Clinic.prototype.email = function () {
        var newWindow = window.open('mailto:' + this.clinic.email);
    };
    return Clinic;
}());
Clinic = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-clinic',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/clinic/clinic.html"*/'<ion-header>\n  <ion-navbar>\n\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'blurred\' : isBlurred}">\n  <div class="mediaSlideContainer">\n    <ion-slides pager slidesPerView="1">\n      <div class="media" [style.background-image]="\'url(\' + (clinic.avatar != \'\' && clinic.avatar != null ? staticUrl+clinic.avatar : \'assets/img/pet.png\' ) + \')\'" style="background-size: cover;"></div>\n    </ion-slides>\n  </div>\n\n  <div *ngIf="loaded">\n   <div class="formContainer">\n    <h5>{{clinic.name}}</h5>\n    <div class="subTitle">Información de la clínica</div>\n    <ion-item>\n      <ion-label fixed>Nombre</ion-label>\n      <ion-input type="text" [(ngModel)]="clinic.name" [disabled]="true"></ion-input>\n    </ion-item>\n    <ion-item>\n\n      <ion-label fixed>Dirección</ion-label>\n      <ion-input type="text" [(ngModel)]="clinic.address" [disabled]="true"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed>Teléfono</ion-label>\n      <ion-input type="text" [(ngModel)]="clinic.phone" [disabled]="true"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed>Sitio web</ion-label>\n      <ion-input type="text" [(ngModel)]="clinic.web" [disabled]="true"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed>Horario:</ion-label>\n      <ion-input type="text" [(ngModel)]="clinic.schedule" [disabled]="true"></ion-input>    \n    </ion-item>\n    <ion-item>\n      <ion-label fixed>Email</ion-label>\n      <ion-input type="text" [(ngModel)]="clinic.email" [disabled]="true"></ion-input>\n    </ion-item>\n\n    <div class="buttonWrapper">\n      <button class="buttonPinkOrange" (click)="call()" ion-button round>Llamar a clínica</button>\n    </div>\n    <div class="buttonWrapper">\n      <button class="button2" (click)="email()" ion-button round>Enviar un email</button>\n    </div>\n\n  </div>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/clinic/clinic.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* PopoverController */]])
], Clinic);

//# sourceMappingURL=clinic.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoctorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DoctorService = (function (_super) {
    __extends(DoctorService, _super);
    function DoctorService(http, storage) {
        var _this = _super.call(this, http, storage) || this;
        _this.http = http;
        _this.storage = storage;
        return _this;
    }
    DoctorService.prototype.getAll = function () {
        return this.get('clinics/');
    };
    return DoctorService;
}(__WEBPACK_IMPORTED_MODULE_3__base_service__["a" /* BaseService */]));
DoctorService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], DoctorService);

//# sourceMappingURL=doctor.service.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsDetalle; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_news_service__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewsDetalle = (function () {
    function NewsDetalle(navCtrl, navParams, socialSharing, newsService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.socialSharing = socialSharing;
        this.newsService = newsService;
        this.image = '';
        this.link = '';
        this.news = null;
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].staticUrl;
        this.news = this.navParams.get("news");
        console.log(this.news);
    }
    NewsDetalle.prototype.ngOnInit = function () {
        var _this = this;
        this.newsService.getFeatured(this.news.featured_media).subscribe(function (data) {
            _this.image = data.guid.rendered;
        }, function (error) {
            console.log(error);
        });
    };
    NewsDetalle.prototype.share = function () {
        var _this = this;
        if (this.isSharing)
            return false;
        this.isSharing = true;
        this.socialSharing.share(this.news.title.rendered, this.news.description, this.image, this.news.link).then(function () {
            _this.isSharing = false;
        }).catch(function () {
            _this.isSharing = false;
        });
    };
    return NewsDetalle;
}());
NewsDetalle = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-newsDetalle',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/home/newsDetalle.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons end>\n      <button class="camera" (click)="share()">\n        <ion-icon name="md-share"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <div class="image" [style.background-image]="\'url(\' + image + \')\'" style="background-size: cover;">\n    <div class="titulo">\n      <p>{{news.title.rendered}}</p>\n      <span class="subtitulo"> {{ news.date | date}}</span>\n    </div>\n  </div>\n\n  <div class="content-html" [innerHtml]="news.content.rendered">\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/home/newsDetalle.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__services_news_service__["a" /* NewsService */]])
], NewsDetalle);

//# sourceMappingURL=newsDetalle.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(310);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export HttpLoaderFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__option_core__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__agm_core__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_barcode_scanner__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_date_picker__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_fcm__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_device__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_component__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_home_newsDetalle__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_list_list__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_pet_pet__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_my_pets_my_pets__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_onboarding_onboarding__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_login_login__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_signup_signup__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_login_code_login_code__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_searching_searching__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_lost_lost__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_owner_owner__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_doctor_doctor__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_clinic_clinic__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_popovers_pet_media_options_pet_media_options__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_popovers_pet_state_pet_state__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_popovers_profile_media_profile_media__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__services_auth_service__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__services_news_service__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__services_doctor_service__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__components_pet_preview_pet_preview__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__components_pet_info_form_pet_info_form__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__components_news_preview_news_preview__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__components_qr_card_qr_card__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__ionic_native_status_bar__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__ionic_native_splash_screen__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__ngx_translate_http_loader__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__modals_ok_ok__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__modals_err_err__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_social_sharing__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_change_password_change_password__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__angular_common__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__ionic_native_email_composer__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__ionic_native_call_number__ = __webpack_require__(294);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















// Pages
















// Popovers



// Services





// Components








// Constant

// Modals







// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
function HttpLoaderFactory(http) {
    return new __WEBPACK_IMPORTED_MODULE_46__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = (function () {
    function AppModule(translate) {
        this.translate = translate;
        this.initTranslate();
    }
    AppModule.prototype.initTranslate = function () {
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang('es-cl');
        this.translate.use('es-cl');
    };
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_pet_pet__["a" /* PetPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_my_pets_my_pets__["a" /* MyPetsPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_home_newsDetalle__["a" /* NewsDetalle */],
            __WEBPACK_IMPORTED_MODULE_32__pages_popovers_pet_media_options_pet_media_options__["a" /* PetMediaOptionsPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_popovers_pet_state_pet_state__["a" /* PetStatePage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_popovers_profile_media_profile_media__["a" /* ProfileMedia */],
            __WEBPACK_IMPORTED_MODULE_22__pages_onboarding_onboarding__["a" /* OnboardingPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* Profile */],
            __WEBPACK_IMPORTED_MODULE_28__pages_owner_owner__["a" /* Owner */],
            __WEBPACK_IMPORTED_MODULE_24__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_login_code_login_code__["a" /* LoginCodePage */],
            __WEBPACK_IMPORTED_MODULE_48__modals_ok_ok__["a" /* ModalOK */],
            __WEBPACK_IMPORTED_MODULE_49__modals_err_err__["a" /* ModalERR */],
            __WEBPACK_IMPORTED_MODULE_26__pages_searching_searching__["a" /* Searching */],
            __WEBPACK_IMPORTED_MODULE_27__pages_lost_lost__["a" /* Lost */],
            __WEBPACK_IMPORTED_MODULE_29__pages_doctor_doctor__["a" /* Doctor */],
            __WEBPACK_IMPORTED_MODULE_30__pages_clinic_clinic__["a" /* Clinic */],
            __WEBPACK_IMPORTED_MODULE_40__components_pet_preview_pet_preview__["a" /* PetPreviewComponent */],
            __WEBPACK_IMPORTED_MODULE_41__components_pet_info_form_pet_info_form__["a" /* PetInfoFormComponent */],
            __WEBPACK_IMPORTED_MODULE_42__components_news_preview_news_preview__["a" /* NewsPreviewComponent */],
            __WEBPACK_IMPORTED_MODULE_43__components_qr_card_qr_card__["a" /* QrCardComponent */],
            __WEBPACK_IMPORTED_MODULE_51__pages_change_password_change_password__["a" /* ChangePasswordPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */], {
                backButtonText: '',
                backButtonIcon: 'ios-arrow-back',
                iconMode: 'ios',
                tabsPlacement: 'bottom',
                pageTransition: 'ios-transition',
                scrollAssist: false,
                autoFocusAssist: false
            }),
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                loader: {
                    provide: __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["a" /* TranslateLoader */],
                    useFactory: HttpLoaderFactory,
                    deps: [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]]
                }
            }),
            __WEBPACK_IMPORTED_MODULE_6__option_core__["b" /* OptionCoreModule */].forRoot(__WEBPACK_IMPORTED_MODULE_47__environments_environment__["a" /* environment */].apiUrl),
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                name: "__mipata",
                driverOrder: ['indexeddb', 'websql']
            }),
            __WEBPACK_IMPORTED_MODULE_8__agm_core__["a" /* AgmCoreModule */].forRoot({
                apiKey: 'AIzaSyAIUiGU1E22l2VqgP5XXEK_Bt36n0eOcxE'
            })
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_pet_pet__["a" /* PetPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_home_newsDetalle__["a" /* NewsDetalle */],
            __WEBPACK_IMPORTED_MODULE_21__pages_my_pets_my_pets__["a" /* MyPetsPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_searching_searching__["a" /* Searching */],
            __WEBPACK_IMPORTED_MODULE_27__pages_lost_lost__["a" /* Lost */],
            __WEBPACK_IMPORTED_MODULE_29__pages_doctor_doctor__["a" /* Doctor */],
            __WEBPACK_IMPORTED_MODULE_30__pages_clinic_clinic__["a" /* Clinic */],
            __WEBPACK_IMPORTED_MODULE_28__pages_owner_owner__["a" /* Owner */],
            __WEBPACK_IMPORTED_MODULE_32__pages_popovers_pet_media_options_pet_media_options__["a" /* PetMediaOptionsPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_popovers_pet_state_pet_state__["a" /* PetStatePage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_popovers_profile_media_profile_media__["a" /* ProfileMedia */],
            __WEBPACK_IMPORTED_MODULE_22__pages_onboarding_onboarding__["a" /* OnboardingPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_login_code_login_code__["a" /* LoginCodePage */],
            __WEBPACK_IMPORTED_MODULE_40__components_pet_preview_pet_preview__["a" /* PetPreviewComponent */],
            __WEBPACK_IMPORTED_MODULE_41__components_pet_info_form_pet_info_form__["a" /* PetInfoFormComponent */],
            __WEBPACK_IMPORTED_MODULE_42__components_news_preview_news_preview__["a" /* NewsPreviewComponent */],
            __WEBPACK_IMPORTED_MODULE_43__components_qr_card_qr_card__["a" /* QrCardComponent */],
            __WEBPACK_IMPORTED_MODULE_48__modals_ok_ok__["a" /* ModalOK */],
            __WEBPACK_IMPORTED_MODULE_49__modals_err_err__["a" /* ModalERR */],
            __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* Profile */],
            __WEBPACK_IMPORTED_MODULE_51__pages_change_password_change_password__["a" /* ChangePasswordPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_35__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_36__services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_37__services_pet_service__["a" /* PetService */],
            __WEBPACK_IMPORTED_MODULE_38__services_news_service__["a" /* NewsService */],
            __WEBPACK_IMPORTED_MODULE_39__services_doctor_service__["a" /* DoctorService */],
            __WEBPACK_IMPORTED_MODULE_44__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_45__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_8__agm_core__["b" /* GoogleMapsAPIWrapper */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_31__pata__["a" /* Pata */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_fcm__["a" /* FCM */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_device__["a" /* Device */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* LOCALE_ID */], useValue: "es-ES" },
            __WEBPACK_IMPORTED_MODULE_50__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_52__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_53__ionic_native_email_composer__["a" /* EmailComposer */],
            __WEBPACK_IMPORTED_MODULE_54__ionic_native_call_number__["a" /* CallNumber */]
        ]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_mergeMap__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_mergeMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_mergeMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromPromise__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromPromise__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UserService = (function (_super) {
    __extends(UserService, _super);
    function UserService(http, storage, device) {
        var _this = _super.call(this, http, storage) || this;
        _this.http = http;
        _this.storage = storage;
        _this.device = device;
        _this.changeAvatar = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* EventEmitter */]();
        _this.push = '';
        return _this;
    }
    UserService.prototype.setPush = function (token) {
        this.push = token;
    };
    UserService.prototype.sendPushToServer = function () {
        var _this = this;
        this.getProfile().subscribe(function (result) {
            _this.post('users/device', {
                id: result.id,
                token: _this.push,
                os: _this.device.platform,
                osversion: _this.device.version,
                appversion: __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].version
            }).subscribe(function (ok) {
                console.log('push ok', ok);
            }, function (err) {
                console.log('push err', err);
            });
        });
    };
    UserService.prototype.getProfile = function () {
        return __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__["Observable"].fromPromise(this.storage.get('MP-Profile'));
    };
    UserService.prototype.getId = function (id) {
        return this.get('users/profile/' + id);
    };
    UserService.prototype.update = function (obj) {
        return this.post('users/profile/update', obj);
    };
    UserService.prototype.region = function () {
        return this.get('region');
    };
    UserService.prototype.distrito = function (region) {
        return this.get('region/' + region + '/district');
    };
    UserService.prototype.changePassword = function (currentPass, newPass) {
        var _this = this;
        return this.getProfile().flatMap(function (result) {
            return _this.post('users/password', { new: newPass, old: currentPass, user_id: result.id });
        });
    };
    return UserService;
}(__WEBPACK_IMPORTED_MODULE_4__base_service__["a" /* BaseService */]));
UserService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__["a" /* Device */]])
], UserService);

//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_profile_profile__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_onboarding_onboarding__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_my_pets_my_pets__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_searching_searching__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_lost_lost__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_doctor_doctor__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_auth_service__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_fcm__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, translate, config, storage, service, alertCtrl, userService, authService, fcm, menu) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.translate = translate;
        this.config = config;
        this.storage = storage;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.userService = userService;
        this.authService = authService;
        this.fcm = fcm;
        this.menu = menu;
        this.rootPage = null;
        this.initializeApp();
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_18__environments_environment__["a" /* environment */].staticUrl;
        // used for an example of ngFor and navigation
        this.pages = [
            { titleTranslationKey: 'APP_MENU.HOME', title: 'Inicio', component: __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */], selected: true },
            { titleTranslationKey: 'APP_MENU.MY_PETS', title: 'Mis mascotas', component: __WEBPACK_IMPORTED_MODULE_10__pages_my_pets_my_pets__["a" /* MyPetsPage */], selected: false },
            { titleTranslationKey: 'APP_MENU.LOOKING_FOR_PARTNERS', title: 'Buscando pareja', component: __WEBPACK_IMPORTED_MODULE_11__pages_searching_searching__["a" /* Searching */], selected: false },
            { titleTranslationKey: 'APP_MENU.LOST_PETS', title: 'Mascotas perdidas', component: __WEBPACK_IMPORTED_MODULE_12__pages_lost_lost__["a" /* Lost */], selected: false },
            { titleTranslationKey: 'APP_MENU.VETERINARY_CLINICS', title: 'Clínicas veterinarias', component: __WEBPACK_IMPORTED_MODULE_13__pages_doctor_doctor__["a" /* Doctor */], selected: false }
        ];
        // Translate page title values
        this.pages.forEach(function (page) {
            _this.translate.get(page.titleTranslationKey).subscribe(function (translation) {
                page.title = translation;
            });
        });
        this.userService.changeAvatar.subscribe(function (st) {
            console.log(st);
            _this.user.avatar = st;
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            setTimeout(function () {
                _this.splashScreen.hide();
            }, 2000);
            if (_this.platform.is('cordova')) {
                _this.platform.registerBackButtonAction(function () {
                    if (_this.menu.isOpen()) {
                        _this.menu.close();
                    }
                    else if (_this.nav.canGoBack()) {
                        _this.nav.pop();
                    }
                    else {
                        //don't do anything
                    }
                });
                _this.fcm.getToken().then(function (token) {
                    //alert('push:'+ token);
                    _this.userService.setPush(token);
                    _this.authService.setPush(token);
                });
                _this.fcm.onNotification().subscribe(function (data) {
                    //alert('Push:' + JSON.stringify(data));
                    if (data.wasTapped) {
                        console.log("Received in background");
                    }
                    else {
                        console.log("Received in foreground");
                    }
                    ;
                });
                _this.fcm.onTokenRefresh().subscribe(function (token) {
                    //alert('push:'+ token);
                    _this.userService.setPush(token);
                    _this.authService.setPush(token);
                });
            }
            _this.authService.loginOk.subscribe(function (data) {
                if (data.profile.avatar == null || data.profile.avatar == "") {
                    data.profile.avatar = "assets/img/default/avatar.png";
                }
                _this.user = data.profile;
                if (data.profile.avatar != "" && data.profile.avatar != null && data.profile.avatar != 'assets/img/default/avatar.png') {
                    _this.user.avatar = _this.staticUrl + _this.user.avatar.replace('/public/', '');
                }
                _this.authService.sendPushToServer(_this.user.id);
            });
            _this.storage.get("MP-FirstTime").then(function (val) {
                if (val == true) {
                    _this.storage.get("MP-FirstTime").then(function (val) {
                        _this.storage.get("token").then(function (token) {
                            if (token) {
                                _this.userService.getProfile().subscribe(function (result) {
                                    _this.user = result;
                                    _this.userService.getId(result.id).subscribe(function (ok) {
                                        _this.user = ok;
                                        if (_this.user.avatar != null && _this.user.avatar != "") {
                                            _this.user.avatar = _this.staticUrl + _this.user.avatar.replace('/public/', '');
                                        }
                                        else {
                                            _this.user.avatar = "assets/img/default/avatar.png";
                                        }
                                        _this.userService.sendPushToServer();
                                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */];
                                    }, function (err) {
                                        _this.service.logError(null, "No fue posible recuperar tu perfil. Por favor accede nuevamente.");
                                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
                                    });
                                }, function (err) {
                                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
                                });
                            }
                            else {
                                _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
                            }
                        });
                    });
                }
                else {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_9__pages_onboarding_onboarding__["a" /* OnboardingPage */];
                }
            });
        });
    };
    MyApp.prototype.profile = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_profile_profile__["a" /* Profile */]);
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
        /*
        this.pages.forEach((p) => {
          p.selected = false;
        });
        page.selected = true;
        */
    };
    MyApp.prototype.closeSession = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Hasta pronto',
            message: 'Deseas salir de tu cuenta',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () { }
                },
                {
                    text: 'Confirmar',
                    handler: function () {
                        _this.storage.remove("token");
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        alert.present();
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/app/app.html"*/'<ion-menu [content]="content" id="leftMenu" class="sidenav">\n\n  <div class="profile" *ngIf="user">\n    <div class="image" [style.background-image]="\'url(\' + user.avatar + \')\'" style="background-size: cover"></div>\n    <div class="name">{{ user.name }}</div>\n    <button menuClose (click)="profile()" ion-button round color="light">{{ \'APP_MENU.MY_ACCOUNT\' | translate }}</button>\n  </div>\n\n  <div menuClose *ngFor="let p of pages" class="link" [ngClass]="{\'selected\' : p.selected}" (click)="openPage(p)">\n    <span>{{p.title}}</span>\n  </div>\n  <div menuClose class="link" [ngClass]="{\'selected\' : selected}" (click)="closeSession()">\n    <span>{{\'APP_MENU.CLOSE\' | translate }}</span>\n  </div>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Config */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_16__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_14__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_15__services_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_17__ionic_native_fcm__["a" /* FCM */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = ListPage_1 = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    return ListPage;
}());
ListPage = ListPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-list',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-left></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-right>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/list/list.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], ListPage);

var ListPage_1;
//# sourceMappingURL=list.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetPreviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_pet_model__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_pet_pet__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the PetPreviewComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var PetPreviewComponent = (function () {
    function PetPreviewComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.isEdit = false;
        this.isMe = false;
        this.add = false;
        this.preventDetail = false;
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].staticUrl;
    }
    PetPreviewComponent.prototype.goToDetail = function () {
        //console.log('gotodetail isEdit: ',this.isEdit);
        if (this.preventDetail == false) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_pet_pet__["a" /* PetPage */], {
                pet: this.pet,
                isDetail: this.isDetail,
                isEdit: this.isEdit,
                isMe: this.isMe
            });
        }
        else {
        }
    };
    PetPreviewComponent.prototype.encodeURL = function (str) {
        return encodeURI(str);
    };
    return PetPreviewComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_pet_model__["a" /* Pet */])
], PetPreviewComponent.prototype, "pet", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetPreviewComponent.prototype, "isDetail", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetPreviewComponent.prototype, "isEdit", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetPreviewComponent.prototype, "isMe", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetPreviewComponent.prototype, "add", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetPreviewComponent.prototype, "preventDetail", void 0);
PetPreviewComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'pet-preview',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/pet-preview/pet-preview.html"*/'<!-- Generated template for the PetPreviewComponent component -->\n<div *ngIf="add">\n  <div class="image" [style.background-image]="\'url(assets/img/add.png)\'" style="background-size: cover;"></div>\n  <div class="name">agregar mascota</div>\n</div>\n<div (click)="goToDetail()" *ngIf="!add">\n  <div class="image" [style.background-image]="\'url(\' + (pet.avatar != \'\' ? encodeURL(staticUrl+pet.avatar) : \'assets/img/default/mascota.png\' ) + \')\'" style="background-size: cover;"></div>\n  <div class="name">{{ pet.name }}</div>\n</div>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/pet-preview/pet-preview.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]])
], PetPreviewComponent);

//# sourceMappingURL=pet-preview.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetInfoFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_pet_model__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_pet_pet__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_my_pets_my_pets__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_owner_owner__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_date_picker__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_barcode_scanner__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var PetInfoFormComponent = (function () {
    function PetInfoFormComponent(service, navCtrl, loadingCtrl, petService, storage, datePicker, platform, barcodeScanner) {
        var _this = this;
        this.service = service;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.petService = petService;
        this.storage = storage;
        this.datePicker = datePicker;
        this.platform = platform;
        this.barcodeScanner = barcodeScanner;
        this.pet = null;
        this.myPet = null;
        this.isEdit = false;
        this.isLove = false;
        this.isMatch = false;
        this.isNew = false;
        this.isLost = false;
        this.isFound = false;
        this.loading = null;
        this.sizes = [];
        this.sexs = [];
        this.colors = [];
        this.especies = [];
        this.razas = [];
        this.names = {
            size: '',
            sex: '',
            color: '',
            especie: '',
            raza: ''
        };
        if (this.pet == null) {
            this.pet = new __WEBPACK_IMPORTED_MODULE_5__models_pet_model__["a" /* Pet */];
        }
        if (this.myPet == null) {
            this.myPet = new __WEBPACK_IMPORTED_MODULE_5__models_pet_model__["a" /* Pet */];
        }
        this.loading = this.loadingCtrl.create();
        this.loading.present();
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].forkJoin([
            this.petService.size(),
            this.petService.color(),
            this.petService.sex(),
            this.petService.especie()
        ])
            .subscribe(function (data) {
            _this.sizes = data[0];
            _this.colors = data[1];
            _this.sexs = data[2];
            _this.especies = data[3];
            if (_this.pet.type) {
                if (_this.pet.type.toString() != "0") {
                    _this.refreshRazas(_this.pet.type);
                }
                _this.names.size = "No definido";
                for (var i = 0; i < _this.sizes.length; i++) {
                    if (_this.sizes[i].id == _this.pet.size) {
                        _this.names.size = _this.sizes[i].value;
                        break;
                    }
                }
                _this.names.color = "No definido";
                for (var i = 0; i < _this.colors.length; i++) {
                    if (_this.colors[i].id == _this.pet.color) {
                        _this.names.color = _this.colors[i].value;
                        break;
                    }
                }
                _this.names.sex = "No definido";
                for (var i = 0; i < _this.sexs.length; i++) {
                    if (_this.sexs[i].id == _this.pet.sex) {
                        _this.names.sex = _this.sexs[i].value;
                        break;
                    }
                }
                _this.names.especie = "No definido";
                for (var i = 0; i < _this.especies.length; i++) {
                    if (_this.especies[i].id == _this.pet.type) {
                        _this.names.especie = _this.especies[i].species;
                        break;
                    }
                }
            }
            _this.loading.dismiss();
        });
    }
    PetInfoFormComponent.prototype.refreshRazas = function (e) {
        var _this = this;
        this.petService.raza(e).subscribe(function (ok) {
            _this.razas = ok;
            for (var i = 0; i < _this.razas.length; i++) {
                if (_this.razas[i].id == _this.pet.breed) {
                    _this.names.raza = _this.razas[i].breed;
                    break;
                }
            }
        });
    };
    PetInfoFormComponent.prototype.setBirthDate = function (oldval) {
        var _this = this;
        var d = new Date();
        if (oldval != "" && oldval != "0000-00-00" && oldval != null) {
            oldval = oldval.split("/").reverse().join("-");
            d = new Date(oldval.replace(/-/g, "/"));
        }
        this.datePicker.show({
            date: d,
            mode: 'date',
            okText: 'Aceptar',
            cancelText: 'Cancelar',
            todayText: '',
            nowText: '',
            allowFutureDates: false,
            doneButtonLabel: 'Aceptar',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
        }).then(function (date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1;
            var yyyy = date.getFullYear();
            var dda = '';
            var mma = '';
            if (dd < 10) {
                dda = '0' + dd;
            }
            else {
                dda = mm.toString();
            }
            if (mm < 10) {
                mma = '0' + mm;
            }
            else {
                mma = mm.toString();
            }
            var newText = dd + '/' + mm + '/' + yyyy;
            _this.pet.birthday = newText;
        }, function (err) { return console.log('Error occurred while getting date: ', err); });
    };
    PetInfoFormComponent.prototype.save = function () {
        var _this = this;
        if (this.pet.name == "" || this.pet.name == null) {
            this.service.logError(null, "Debe indicar nombre de mascota");
        }
        else {
            var loading_1 = this.loadingCtrl.create({
                content: 'guardando...'
            });
            this.pet.name = this.pet.name.toString().trim();
            loading_1.present();
            this.storage.get('MP-Profile').then(function (val) {
                var saveOperation = _this.petService.update(_this.pet.id, _this.pet);
                saveOperation.subscribe(function (ok) {
                    loading_1.dismiss();
                    if (ok.hasOwnProperty("id")) {
                        _this.service.showOk();
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_my_pets_my_pets__["a" /* MyPetsPage */]);
                    }
                    else {
                        _this.service.logError(null, "No fue posible guardar su mascota, recuerde completar todos los campos");
                    }
                }, function (error) {
                    loading_1.dismiss();
                    _this.service.logError(null, "No fue posible guardar su mascota, recuerde completar todos los campos");
                    console.log(error);
                });
            });
        }
    };
    PetInfoFormComponent.prototype.add = function () {
        var _this = this;
        if (this.pet.name == "" || this.pet.name == null) {
            this.service.logError(null, "Debe indicar nombre de mascota");
        }
        else if (this.pet.breed == "" || this.pet.breed == null) {
            this.service.logError(null, "Debe indicar raza de mascota");
        }
        else {
            var loading_2 = this.loadingCtrl.create({
                content: 'guardando...'
            });
            loading_2.present();
            this.pet.name = this.pet.name.toString().trim();
            this.storage.get('MP-Profile').then(function (val) {
                var addOperation = _this.petService.add(val.id, _this.pet);
                addOperation.subscribe(function (ok) {
                    loading_2.dismiss();
                    if (ok.hasOwnProperty("id")) {
                        _this.service.showOk();
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_my_pets_my_pets__["a" /* MyPetsPage */]);
                    }
                    else {
                        _this.service.logError(null, "No fue posible guardar su mascota, recuerde completar todos los campos");
                    }
                }, function (error) {
                    loading_2.dismiss();
                    _this.service.logError(null, "No fue posible guardar su mascota, recuerde completar todos los campos");
                    console.log(error);
                });
            });
        }
    };
    PetInfoFormComponent.prototype.rechazar = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        var accept = this.petService.reject(this.pet.id);
        accept.subscribe(function (ok) {
            loading.dismiss();
            _this.service.showOk();
            _this.navCtrl.pop();
        }, function (err) {
            loading.dismiss();
            _this.service.logError(null, "No fue posible rechazar, es posible que ya no esté disponible");
        });
    };
    PetInfoFormComponent.prototype.notificar = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        console.log('myPetId.form:' + JSON.stringify(this.myPet));
        var accept = this.petService.notifyOwner(this.pet.id, this.petId);
        accept.subscribe(function (ok) {
            loading.dismiss();
            _this.service.showOk();
            _this.navCtrl.pop();
        }, function (err) {
            loading.dismiss();
            _this.service.logError(null, "No fue posible aplicar al candidato, es posible que ya no esté disponible");
        });
    };
    PetInfoFormComponent.prototype.aceptar = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        var accept = this.petService.accept(this.pet.id);
        accept.subscribe(function (ok) {
            loading.dismiss();
            _this.service.showOk();
            _this.navCtrl.pop();
        }, function (err) {
            loading.dismiss();
            _this.service.logError(null, "No fue posible aplicar al candidato, es posible que ya no esté disponible");
        });
    };
    PetInfoFormComponent.prototype.owner = function (owner) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_owner_owner__["a" /* Owner */], { owner: owner });
    };
    PetInfoFormComponent.prototype.scanear = function () {
        var _this = this;
        if (this.platform.is('cordova')) {
            this.barcodeScanner.scan().then(function (barcodeData) {
                if (barcodeData.text.indexOf('mimaskot.pe') > -1) {
                    var data = barcodeData.text.split('/');
                    var code = data[data.length - 1].trim();
                    _this.loading = _this.loadingCtrl.create();
                    _this.loading.present();
                    var petcheck = _this.petService.getMascotByQR(code);
                    petcheck.subscribe(function (pet) {
                        if (pet) {
                            // ir al detalle de la mascota encontrada
                            _this.service.showOk('Enhorabuena, haz encontrado una mascota que está extraviada. Hemos enviado una notificación a su dueño, si lo deseas puedes contactarlo tú también.');
                            _this.goToPetFound(pet);
                        }
                        else {
                            _this.loading.dismiss();
                            _this.service.logError({}, 'El QR leidó está asignado a otra mascota');
                        }
                    }, function (err) {
                        _this.loading.dismiss();
                        _this.service.logError({}, 'No fue posible validar el QR');
                    });
                }
                else if (barcodeData.text.length > 0) {
                    _this.service.logError({}, 'QR escaneado no corresponde a MiMaskot');
                }
                else {
                    // did not take the qr code
                }
            }, function (err) {
                _this.service.logError(null, "Servicio temporalmente no disponible");
            });
        }
    };
    PetInfoFormComponent.prototype.goToPetFound = function (pet) {
        if (pet.hasOwnProperty("status")) {
            this.loading.dismiss();
            this.service.logError({}, 'Código QR no encontrado como una mascota perdida');
        }
        else {
            this.loading.dismiss();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_pet_pet__["a" /* PetPage */], {
                pet: pet,
                isDetail: 0,
                isLove: false,
                isLost: true,
                isEdit: false,
                isFound: true
            });
        }
    };
    return PetInfoFormComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__models_pet_model__["a" /* Pet */])
], PetInfoFormComponent.prototype, "pet", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__models_pet_model__["a" /* Pet */])
], PetInfoFormComponent.prototype, "myPet", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Number)
], PetInfoFormComponent.prototype, "petId", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetInfoFormComponent.prototype, "isDetail", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetInfoFormComponent.prototype, "isEdit", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetInfoFormComponent.prototype, "isLove", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetInfoFormComponent.prototype, "isMatch", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetInfoFormComponent.prototype, "isNew", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetInfoFormComponent.prototype, "isLost", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], PetInfoFormComponent.prototype, "isFound", void 0);
PetInfoFormComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'pet-info-form',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/pet-info-form/pet-info-form.html"*/'<!-- Generated template for the PetInfoFormComponent component -->\n<div class="formContainer">\n  <div class="subTitle" *ngIf="!isDetail">{{ \'PET.PET_INFORMATION\' | translate }}</div>\n  <div class="subTitle" *ngIf="isDetail">{{ \'PET.MY_PET_INFORMATION\' | translate }}</div>\n  <ion-item>\n    <ion-label fixed>{{ \'PET.NAME\' | translate }}</ion-label>\n    <ion-input type="text" [(ngModel)]="pet.name" [disabled]="true" [hidden]="isEdit"></ion-input>\n    <ion-input type="text" [(ngModel)]="pet.name" [hidden]="!isEdit"></ion-input>\n  </ion-item>\n  <ion-item>\n\n    <ion-label fixed>{{ \'PET.SPECIES\' | translate }}</ion-label>\n    <ion-input type="text" [(ngModel)]="names.especie" [disabled]="true" [hidden]="isEdit"></ion-input>\n    <ion-select [(ngModel)]="pet.type" (ionChange)="refreshRazas($event)" [hidden]="!isEdit">\n      <ion-option *ngFor="let species of especies" [value]="species.id">{{species.species}}</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label fixed>{{ \'PET.BREED\' | translate }}</ion-label>\n    <ion-input type="text" [(ngModel)]="names.raza" [disabled]="true" [hidden]="isEdit"></ion-input>\n    <ion-select [(ngModel)]="pet.breed" [hidden]="!isEdit">\n      <ion-option *ngFor="let breed of razas" [value]="breed.id">{{breed.breed}}</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label fixed>{{ \'PET.BIRTH_DATE\' | translate }}</ion-label>\n\n    <ion-input type="text" [(ngModel)]="pet.birthday" [disabled]="true" [hidden]="isEdit"></ion-input>\n    <ion-input type="text" [(ngModel)]="pet.birthday" readonly [hidden]="!isEdit" (click)="setBirthDate(pet.birthday)"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label fixed>{{ \'PET.GENDER\' | translate }}</ion-label>\n    <ion-input type="text" [(ngModel)]="names.sex" [disabled]="true" [hidden]="isEdit"></ion-input>\n    <ion-select [(ngModel)]="pet.sex" [hidden]="!isEdit">\n      <ion-option *ngFor="let sex of sexs" [value]="sex.id">{{sex.value}}</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label fixed>{{ \'PET.COLOR\' | translate }}</ion-label>\n    <ion-input type="text" [(ngModel)]="names.color" [disabled]="true" [hidden]="isEdit"></ion-input>\n    <ion-select [(ngModel)]="pet.color" [hidden]="!isEdit">\n      <ion-option *ngFor="let color of colors" [value]="color.id">{{color.value}}</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label fixed>{{ \'PET.SIZE\' | translate }}</ion-label>\n    <ion-input type="text" [(ngModel)]="names.size" [disabled]="true" [hidden]="isEdit"></ion-input>\n    <ion-select [(ngModel)]="pet.size" [hidden]="!isEdit">\n      <ion-option *ngFor="let size of sizes" [value]="size.id">{{size.value}}</ion-option>\n    </ion-select>\n  </ion-item>\n  <!--\n  <div class="buttonWrapper" *ngIf="isEdit ">\n    <button class="buttonPinkOrange" ion-button round>{{ \'PET.UPDATE_INFO\' | translate }}</button>\n  </div>\n  -->\n  <div class="buttonWrapper" *ngIf="isNew">\n    <button class="buttonPinkOrange" (click)="add()" ion-button round>{{ \'PET.SAVE\' | translate }}</button>\n  </div>\n\n  <div class="buttonWrapper" *ngIf="isEdit && !isNew">\n    <button class="buttonPinkOrange" (click)="save()" ion-button round>{{ \'PET.UPDATE_INFO\' | translate }}</button>\n  </div>\n\n  <div class="buttonWrapper" *ngIf="isLove && !isMatch">\n    <button class="buttonPinkOrange" (click)="notificar()" ion-button round>Notificar al dueño</button>\n  </div>\n\n  <div class="buttonWrapper" *ngIf="isMatch">\n    <button class="buttonPinkOrange" (click)="aceptar()" ion-button round>Aceptar candidato</button>\n  </div>\n  <div class="buttonWrapper" *ngIf="isMatch">\n    <button class="button2" (click)="rechazar()" ion-button round>Rechazar candidato</button>\n  </div>\n\n  <div class="buttonWrapper" *ngIf="isLost && isFound && !isDetail">\n    <button class="buttonPinkOrange" (click)="owner(pet.user_id)" ion-button round>Ver datos del dueño</button>\n  </div>\n\n  <div class="buttonWrapper" *ngIf="isLost && !isFound && !isDetail">\n    <button class="buttonPinkOrange" (click)="scanear()" ion-button round>Escanear Mascota</button>\n  </div>\n</div>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/pet-info-form/pet-info-form.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_9__services_pet_service__["a" /* PetService */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_10__ionic_native_date_picker__["a" /* DatePicker */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_11__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
], PetInfoFormComponent);

//# sourceMappingURL=pet-info-form.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPreviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_news_model__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_home_newsDetalle__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_news_service__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the NewsPreviewComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var NewsPreviewComponent = (function () {
    function NewsPreviewComponent(navCtrl, newsService) {
        this.navCtrl = navCtrl;
        this.newsService = newsService;
        this.image = '';
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].staticUrl;
    }
    NewsPreviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('A::' + this.news.featured_media);
        this.newsService.getFeatured(this.news.featured_media).subscribe(function (data) {
            _this.image = data.guid.rendered;
        }, function (error) {
            console.log(error);
        });
    };
    NewsPreviewComponent.prototype.detalleNews = function (obj) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_home_newsDetalle__["a" /* NewsDetalle */], {
            news: this.news
        });
    };
    return NewsPreviewComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_news_model__["a" /* News */])
], NewsPreviewComponent.prototype, "news", void 0);
NewsPreviewComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'news-preview',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/news-preview/news-preview.html"*/'<!-- Generated template for the NewsPreviewComponent component -->\n<div class="wrapper">\n  <div class="image" [style.background-image]="\'url(\' + image + \')\'" style="background-size: cover;" (click)="detalleNews(news);">\n    <div class="descriptionContainer">\n      <div class="title">{{ news.title.rendered }}</div>\n      <div class="date">{{ news.date | date }}</div>\n    </div>\n  </div>\n</div>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/news-preview/news-preview.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__services_news_service__["a" /* NewsService */]])
], NewsPreviewComponent);

//# sourceMappingURL=news-preview.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return News; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__option_core__ = __webpack_require__(143);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var News = (function (_super) {
    __extends(News, _super);
    function News() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    News.prototype.decode = function (jsonObject) {
        this.image = jsonObject['image'];
        this.title = jsonObject['title'];
        if (jsonObject['date']) {
            this.created_at = new Date(jsonObject['date']);
            this.normalizeDateToUsersLocale(this.created_at);
        }
    };
    News.prototype.getFormEntityName = function () {
        return 'news';
    };
    News.prototype.normalizeDateToUsersLocale = function (date) {
        date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
    };
    return News;
}(__WEBPACK_IMPORTED_MODULE_0__option_core__["a" /* OptEntity */]));

//# sourceMappingURL=news.model.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QrCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_pet_model__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_pet_service__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the QrCardComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var QrCardComponent = (function () {
    function QrCardComponent(barcodeScanner, service, petService, loadingCtrl, platform) {
        this.barcodeScanner = barcodeScanner;
        this.service = service;
        this.petService = petService;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.nueva = false;
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* EventEmitter */]();
        this.loading = null;
    }
    QrCardComponent.prototype.scanear = function () {
        var _this = this;
        if (this.platform.is('ios') || this.platform.is('android')) {
            this.barcodeScanner.scan().then(function (barcodeData) {
                if (barcodeData.text.indexOf('mimaskot.pe') > -1) {
                    var data = barcodeData.text.split('/');
                    var code = data[data.length - 1].trim();
                    _this.loading = _this.loadingCtrl.create();
                    _this.loading.present();
                    var petcheck = _this.petService.uniqueQR(code);
                    petcheck.subscribe(function (ok) {
                        if (ok.status == 1) {
                            _this.okQr(code);
                        }
                        else {
                            _this.loading.dismiss();
                            _this.service.logError({}, 'El QR leidó está asignado a otra mascota');
                        }
                    }, function (err) {
                        _this.loading.dismiss();
                        _this.service.logError({}, 'No fue posible validar el QR');
                    });
                }
                else {
                    _this.service.logError({}, 'QR escaneado no corresponde a MiMaskot');
                }
            }, function (err) {
                _this.service.logError({}, 'Error al internar abrir la cámara');
            });
        }
        else {
            var code = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 8).toUpperCase();
            this.loading = this.loadingCtrl.create();
            this.loading.present();
            this.okQr(code);
        }
    };
    QrCardComponent.prototype.okQr = function (codex) {
        var _this = this;
        this.pet.code = codex;
        this.pet.qr = "https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=" + codex + "&chld=H|1";
        if (this.nueva == false) {
            var scode = this.petService.setCode(this.pet.id, codex);
            scode.subscribe(function (ok) {
                _this.loading.dismiss();
                _this.setOK(true, codex);
            }, function (err) {
                _this.loading.dismiss();
                _this.service.logError({}, 'No fue posible actualizar el QR');
            });
        }
        else {
            this.loading.dismiss();
            this.setOK(true, codex);
        }
    };
    QrCardComponent.prototype.setOK = function (ok, identifier) {
        this.change.emit({ ok: ok, identifier: identifier });
    };
    QrCardComponent.prototype.getQR = function (codez) {
        return "https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=" + codez + "&chld=H|1";
    };
    return QrCardComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__models_pet_model__["a" /* Pet */])
], QrCardComponent.prototype, "pet", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Boolean)
], QrCardComponent.prototype, "nueva", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Output */])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* EventEmitter */])
], QrCardComponent.prototype, "change", void 0);
QrCardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'qr-card',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/qr-card/qr-card.html"*/'<!-- Generated template for the QrCardComponent component -->\n<div class="subTitle white">{{ \'PET.ID_MIMASKOT\' | translate }}</div>\n<div class="qrCard">\n  <img class="qr" *ngIf="pet.code" [src]="getQR(pet.code)" alt="qr">\n  <div class="qr emptyQr" *ngIf="!pet.code"></div>\n  <div class="identifierWrapper">\n    <div>{{ \'PET.IDENTIFIER\' | translate }}</div>\n    <div class="identifier">{{ pet.code }}</div>\n  </div>\n  <img id="mimaskotLogo" src="./assets/img/logo_mimaskot.png" alt="Mimaskot">\n  <button ion-button round (click)="scanear()" outline *ngIf="nueva">{{ \'PET.SCAN\' | translate }}</button>\n  <button ion-button round (click)="scanear()" outline *ngIf="!nueva">{{ \'PET.SCAN_AGAIN\' | translate }}</button>\n</div>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/components/qr-card/qr-card.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
        __WEBPACK_IMPORTED_MODULE_3__pata__["a" /* Pata */],
        __WEBPACK_IMPORTED_MODULE_5__services_pet_service__["a" /* PetService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
], QrCardComponent);

//# sourceMappingURL=qr-card.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_news_service__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = (function () {
    function HomePage(navCtrl, petService, newsService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.petService = petService;
        this.newsService = newsService;
        this.pets = [];
        this.news = [];
        this.pageNews = 1;
        this.perPegeNews = 2;
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].staticUrl;
        this.petService.getLast().subscribe(function (pets) { return _this.pets = pets; });
        this.newsService.geList(this.pageNews, this.perPegeNews).subscribe(function (news) {
            _this.pageNews++;
            _this.news = news;
        }, function (error) {
            console.log(error);
        });
    }
    HomePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.pageNews = 1;
        this.petService.getLast().subscribe(function (pets) { return _this.pets = pets; });
        this.newsService.geList(this.pageNews, this.perPegeNews).subscribe(function (news) {
            _this.pageNews++;
            _this.news = news;
            console.log(news);
            refresher.complete();
        }, function (error) {
            console.log(error);
            refresher.complete();
        });
    };
    HomePage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('doInfinite...');
        this.newsService.geList(this.pageNews, this.perPegeNews).subscribe(function (news) {
            _this.pageNews++;
            news.forEach(function (value) {
                _this.news.push(value);
            });
            infiniteScroll.complete();
        }, function (error) {
            console.log(error);
            infiniteScroll.complete();
        });
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <span class="icon-menu"></span>\n    </button>\n    <ion-title>{{ \'APP_MENU.HOME\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <div class="subTitle centered">{{ \'HOME.LAST_MEMBERS_OF_OUR_COMMUNITY\' | translate }}</div>\n  <div class="slidesWrapper">\n    <ion-slides pager slidesPerView="4.5">\n      <ion-slide *ngFor="let pet of pets" style="width: 80px;">\n        <pet-preview [pet]="pet" [isDetail]="false" [isEdit]="false"></pet-preview>\n      </ion-slide>\n    </ion-slides>\n    <div text-center *ngIf="pets.length == 0">\n      No hay nuevos integrantes\n    </div>\n  </div>\n\n  <!-- start Banner -->\n  <ins data-revive-zoneid="9" data-revive-id="9d276fa041e1067786e857fe85ab98f9"></ins>\n  <!-- end Banner -->\n\n  <div class="subTitle">{{ \'HOME.NEWS\' | translate }}</div>\n  <news-preview *ngFor="let n of news" [news]="n"></news-preview>\n\n	<ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n	<ion-infinite-scroll-content></ion-infinite-scroll-content>\n	</ion-infinite-scroll>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__services_pet_service__["a" /* PetService */],
        __WEBPACK_IMPORTED_MODULE_3__services_news_service__["a" /* NewsService */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = (function () {
    function AuthService(http, storage, device) {
        this.http = http;
        this.storage = storage;
        this.device = device;
        this.loginOk = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* EventEmitter */]();
        this.push = '';
    }
    /** login */
    AuthService.prototype.login = function (params) {
        var _this = this;
        var req = this.http.post(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].apiUrl + 'auth/login', params);
        req.subscribe(function (data) {
            _this.loginOk.emit(data);
        });
        return req;
    };
    /** signup */
    AuthService.prototype.signup = function (params) {
        var req = this.http.post(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].apiUrl + 'users/register', params);
        return req;
    };
    AuthService.prototype.setToken = function (token) {
        this.storage.set('token', token);
    };
    /** verification sms */
    AuthService.prototype.verifyPhone = function (params) {
        var req = this.http.post(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].apiUrl + 'users/verification', params);
        return req;
    };
    AuthService.prototype.setPush = function (token) {
        this.push = token;
    };
    AuthService.prototype.sendPushToServer = function (id) {
        this.http.post(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].apiUrl + 'users/device', {
            id: id,
            token: this.push,
            os: this.device.platform,
            osversion: this.device.version,
            appversion: __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].version
        }).subscribe(function (ok) {
            console.log('push ok', ok);
        }, function (err) {
            console.log('push err', err);
        });
    };
    /** logout */
    AuthService.prototype.logout = function () { };
    return AuthService;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Output */])(),
    __metadata("design:type", Object)
], AuthService.prototype, "loginOk", void 0);
AuthService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__["a" /* Device */]])
], AuthService);

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Pet; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__option_core__ = __webpack_require__(143);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Pet = (function (_super) {
    __extends(Pet, _super);
    function Pet() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Pet.prototype.decode = function (jsonObject) {
        this.id = jsonObject['id'];
        this.name = jsonObject['name'];
        this.color = jsonObject['color'];
        this.size = jsonObject['size'];
        this.qr = jsonObject['qr'];
        this.identifier = jsonObject['identifier'];
        this.extraInfo = jsonObject['extraInfo'];
        this.principalMedia = jsonObject['principalMedia'];
        this.additionalImages = jsonObject['additionalImages'];
        this.code = jsonObject['code'];
        this.avatar = jsonObject['avatar'];
        this.media = jsonObject['media'];
        this.missing = jsonObject['missing'];
        this.available = jsonObject['available'];
        this.type = jsonObject['type'];
        this.sex = jsonObject['sex'];
        this.breed = jsonObject['breed'];
        /*
        if (jsonObject['birthday']) {
          this.birthday = new Date(jsonObject['created']);
          this.normalizeDateToUsersLocale(this.birthday);
        }
        */
    };
    Pet.prototype.getFormEntityName = function () {
        return 'pet';
    };
    Pet.prototype.normalizeDateToUsersLocale = function (date) {
        date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
    };
    return Pet;
}(__WEBPACK_IMPORTED_MODULE_0__option_core__["a" /* OptEntity */]));

//# sourceMappingURL=pet.model.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popovers_pet_media_options_pet_media_options__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__popovers_pet_state_pet_state__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_user_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_pet_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_owner_owner__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pata__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environments_environment__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the PetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

var PetPage = (function () {
    function PetPage(navCtrl, navParams, petService, userService, loadingCtrl, popoverCtrl, storage, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.petService = petService;
        this.userService = userService;
        this.loadingCtrl = loadingCtrl;
        this.popoverCtrl = popoverCtrl;
        this.storage = storage;
        this.service = service;
        this.imagenIndex = 0;
        this.isDetail = true;
        this.isEdit = false;
        this.isLove = false; // buscando pareja
        this.isMatch = false; // es candidato de la mascota
        this.isLost = false;
        this.isFound = false;
        this.isBlurred = false;
        this.loaded = false;
        this.lastView = false;
        this.imagenes = [];
        this.loading = null;
        this.lastSeen = false;
        this.lastSeenID = 0;
        this.lastSeenName = '';
        this.lastSeenLat = 0;
        this.lastSeenLng = 0;
        this.styles = [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#6195a0" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [{ "color": "#e7e5e3" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#e6f3d6" }, { "visibility": "on" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": "-100" }, { "lightness": 45 }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#e3d6c7" }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "elementType": "labels.text", "stylers": [{ "color": "#4e4e4e" }] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{ "color": "#f4f4f4" }] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "color": "#787878" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#eaf6f8" }, { "visibility": "on" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#eaf6f8" }] }];
        this.staticUrl = __WEBPACK_IMPORTED_MODULE_9__environments_environment__["a" /* environment */].staticUrl;
        this.pet = navParams.get('pet');
        if (navParams.get('myPet')) {
            this.myPet = navParams.get('myPet');
            console.log('inPet.my: ' + JSON.stringify(this.myPet));
        }
        if (navParams.get('petId')) {
            this.petId = navParams.get('petId');
            console.log('petId.my: ' + JSON.stringify(this.petId));
        }
        this.isEdit = navParams.get('isEdit');
        this.isDetail = navParams.get('isDetail');
        if (navParams.get('isLove')) {
            this.isLove = navParams.get('isLove');
        }
        if (navParams.get('lastView')) {
            this.lastView = navParams.get('lastView');
        }
        if (navParams.get('isMatch')) {
            this.isMatch = navParams.get('isMatch');
        }
        if (navParams.get('isLost')) {
            this.isLost = navParams.get('isLost');
        }
        if (navParams.get('isFound')) {
            this.isFound = navParams.get('isFound');
        }
        this.loaded = true;
        //console.log(this.pet.additionalImages);
        if (this.pet.additionalImages != null && this.pet.additionalImages != "") {
            var ex = this.pet.additionalImages.split(',');
            for (var i = 0; i < ex.length; i++) {
                this.imagenes.push(ex[i]);
            }
        }
        if (this.lastView == true) {
            var spet = this.petService.getLostPetByQR(this.pet.code);
            spet.subscribe(function (data) {
                if (data.hasOwnProperty("status") && data.status == false) {
                    _this.lastSeen = false;
                }
                else {
                    _this.lastSeen = true;
                    _this.lastSeenLat = data.missingPet.lat_seen;
                    _this.lastSeenLng = data.missingPet.lng_seen;
                    _this.lastSeenID = data.missingPet.user_id;
                    var suser = _this.userService.getId(data.missingPet.user_id);
                    suser.subscribe(function (data2) {
                        _this.lastSeenName = data2.name + ' ' + data2.last_name;
                    });
                }
            });
        }
    }
    PetPage.prototype.loadAPIWrapper = function (map) {
        this.map = map;
    };
    PetPage.prototype.removeBlur = function () {
        this.isBlurred = false;
    };
    PetPage.prototype.gotoUser = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_owner_owner__["a" /* Owner */], { owner: id });
    };
    PetPage.prototype.getmarkericon = function () {
        return 'assets/img/marker.png';
    };
    PetPage.prototype.presentMediaOptionsPopover = function (event) {
        var _this = this;
        this.imagenIndex = this.slides.getActiveIndex();
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popovers_pet_media_options_pet_media_options__["a" /* PetMediaOptionsPage */], { imagenIndex: this.imagenIndex, additionalImages: this.pet.additionalImages, id: this.pet.id });
        popover.present({
            ev: event
        });
        popover.onDidDismiss(function (data) {
            if (data && data.hasOwnProperty('reload') && data.reload == true) {
                _this.reloadPet();
            }
            _this.removeBlur();
        });
        this.isBlurred = true;
    };
    PetPage.prototype.reloadPet = function () {
        //this.loading = this.loadingCtrl.create({content:'subiendo foto...'});
        //this.loading.present();
        var _this = this;
        this.storage.get('MP-Profile').then(function (val) {
            _this.petService.getId(val.id).subscribe(function (pets) {
                for (var i = 0; i < pets.length; i++) {
                    if (pets[i].id == _this.pet.id) {
                        _this.pet = pets[i];
                    }
                }
                //this.loading.dismiss();
            });
        });
    };
    PetPage.prototype.presentStatePopover = function (event) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__popovers_pet_state_pet_state__["a" /* PetStatePage */], { id: this.pet.id, petMissing: this.pet.missing, petLoving: this.pet.available });
        popover.present({
            ev: event
        });
        popover.onDidDismiss(function (data) {
            if (data && data.hasOwnProperty("petLoving")) {
                _this.pet.available = (data.petLoving == true ? 1 : 0);
            }
            if (data && data.hasOwnProperty("petMissing")) {
                _this.pet.missing = (data.petMissing == true ? 1 : 0);
            }
            _this.removeBlur();
        });
        this.isBlurred = true;
    };
    PetPage.prototype.reportarComoEncontrado = function (code) {
        var _this = this;
        this.petService.petFound(code.trim()).subscribe(function (data) {
            _this.service.showOk();
            _this.navCtrl.pop();
        }, function (err) {
            _this.service.logError(null, "No fue posible reportar la mascota, intentalo nuevamente");
        });
    };
    return PetPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
], PetPage.prototype, "slides", void 0);
PetPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-pet',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/pet/pet.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons end style="margin-right: 20px;">\n    <button (click)="presentMediaOptionsPopover($event)" *ngIf="isDetail && !lastView" class="buttonPinkOrange camera">\n      <ion-icon ios="ios-camera-outline" md="ios-camera-outline"></ion-icon>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'blurred\' : isBlurred}">\n  <div class="mediaSlideContainer">\n    <ion-slides #mySlider pager slidesPerView="1">\n      <ion-slide *ngFor="let imagen of imagenes">\n        <div class="media" [style.background-image]="\'url(\' + staticUrl+imagen + \')\'" style="background-size: cover;"></div>\n      </ion-slide>\n      <div class="media" *ngIf="imagenes.length == 0" [style.background-image]="\'url(\' + (pet.avatar != \'\' && pet.avatar != null ? staticUrl+pet.avatar : \'assets/img/pet.png\' ) + \')\'" style="background-size: cover;"></div>\n    </ion-slides>\n  </div>\n  <div class="name">\n    {{ pet.name }}\n    <button *ngIf="isDetail && !lastView" ion-button round class="options buttonPinkOrange" (click)="presentStatePopover($event)">{{ \'PET.EDIT_STATE\' | translate }}</button>\n  </div>\n  <div *ngIf="loaded && !lastView">\n    <qr-card *ngIf="isDetail && !isLost" [pet]="pet"></qr-card>\n    <pet-info-form [pet]="pet" [isDetail]="isDetail" [isLost]="isLost" [isLove]="isLove" [isMatch]="isMatch" [isEdit]="isEdit" [isFound]="isFound" [myPet]="myPet" [petId]="petId"></pet-info-form>\n  </div>\n\n  <div *ngIf="lastView" class="lastView">\n    <strong>PERDIDO EL {{pet.missing_at | date: \'dd/MM/yyyy\'}}</strong>\n    <br /><br />\n    <div *ngIf="lastSeen">\n      Tu mascota fue vista por última vez por {{lastSeenName}}. Puedes ver su información y contactarlo para saber más.\n      <br /><br />\n      <agm-map [latitude]="lastSeenLat" [longitude]="lastSeenLng" [styles]="styles" [zoom]="13" [zoomControl]="false" [streetViewControl]="false" (onMapLoad)=\'loadAPIWrapper($event)\'>\n        <agm-marker [iconUrl]="getmarkericon()" [latitude]="lastSeenLat" [longitude]="lastSeenLng">\n        </agm-marker>\n      </agm-map>\n      <br /><br />\n      <div class="buttonWrapper">\n        <button class="buttonPinkOrange" (click)="gotoUser(lastSeenID)" ion-button round>Ver información de contacto</button>\n      </div>\n    </div>\n\n\n    <div *ngIf="!lastSeen">\n    Lo sentimos, tu mascota aún no ha sido vista por nadie\n    </div>\n    <br /><br />\n    Si encontraste esta mascota y tiene su collar MiMaskot haz clic en el botón para notificar a su dueño.\n    <br /><br />\n    <button class="buttonWhiting" (click)="reportarComoEncontrado(pet.code)" ion-button round>Reportar como encontrado</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/pet/pet.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__services_pet_service__["a" /* PetService */],
        __WEBPACK_IMPORTED_MODULE_5__services_user_service__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* PopoverController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_8__pata__["a" /* Pata */]])
], PetPage);

//# sourceMappingURL=pet.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_mergeMap__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_mergeMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_mergeMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_fromPromise__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_fromPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_fromPromise__);





var BaseService = (function () {
    function BaseService(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    /** get token */
    BaseService.prototype.getAuthToken = function () {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].fromPromise(this.storage.get('token'));
    };
    /** GET */
    BaseService.prototype.get = function (url, body) {
        var _this = this;
        return this.getAuthToken().flatMap(function (token) {
            return _this.http.get(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].apiUrl + url, {
                params: body,
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().append('Authorization', "Bearer " + token)
            });
        });
    };
    /** POST */
    BaseService.prototype.post = function (url, body) {
        var _this = this;
        return this.getAuthToken().flatMap(function (token) {
            return _this.http.post(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].apiUrl + url, body, {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().append('Authorization', "Bearer " + token)
            });
        });
    };
    /** PUT */
    BaseService.prototype.put = function (url, body) {
        var _this = this;
        return this.getAuthToken().flatMap(function (token) {
            return _this.http.put(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].apiUrl + url, body, {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().append('Authorization', "Bearer " + token)
            });
        });
    };
    /** DELETE */
    BaseService.prototype.delete = function (url, body) {
        var _this = this;
        return this.getAuthToken().flatMap(function (token) {
            return _this.http.delete(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].apiUrl + url, {
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().append('Authorization', "Bearer " + token)
            });
        });
    };
    return BaseService;
}());

//# sourceMappingURL=base.service.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_mergeMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_fromPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__(12);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NewsService = (function (_super) {
    __extends(NewsService, _super);
    function NewsService(http, storage) {
        var _this = _super.call(this, http, storage) || this;
        _this.http = http;
        _this.storage = storage;
        return _this;
    }
    NewsService.prototype.geList = function (page, perPage) {
        //return this.get('news/');
        var apiNewsUrl = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].newsApi + 'noticias/?page=' + page + '&per_page=' + perPage + '&status=publish';
        return this.http.get(apiNewsUrl, {
            params: null
        });
    };
    NewsService.prototype.getFeatured = function (id) {
        var apiMediaUrl = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].newsApi + 'media/';
        return this.http.get(apiMediaUrl + id, {
            params: null
        });
    };
    return NewsService;
}(__WEBPACK_IMPORTED_MODULE_3__base_service__["a" /* BaseService */]));
NewsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], NewsService);

//# sourceMappingURL=news.service.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyPetsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_pet_model__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_pet_service__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyPetsPage = (function () {
    function MyPetsPage(navCtrl, navParams, petService, storage, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.petService = petService;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.activePet = null;
        this.pets = [];
        this.newPet = new __WEBPACK_IMPORTED_MODULE_3__models_pet_model__["a" /* Pet */]({});
        this.addPetForm = false;
        this.addPetForm2 = false;
        this.loading = null;
        this.loadok = false;
        this.loadok = false;
    }
    MyPetsPage.prototype.ionViewWillEnter = function () {
        this.refreshMyPets();
    };
    MyPetsPage.prototype.refreshMyPets = function () {
        var _this = this;
        this.loadok = false;
        this.loading = this.loadingCtrl.create();
        this.loading.present();
        this.storage.get('MP-Profile').then(function (val) {
            _this.petService.getId(val.id).subscribe(function (pets) { _this.pets = pets; _this.loadok = true; _this.loading.dismiss(); });
        });
    };
    MyPetsPage.prototype.qrChange = function (obj) {
        this.addPetForm2 = obj.ok;
        this.newPet.code = obj.identifier;
    };
    MyPetsPage.prototype.addPet = function () {
        this.addPetForm = true;
    };
    return MyPetsPage;
}());
MyPetsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'page-my-pets',template:/*ion-inline-start:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/my-pets/my-pets.html"*/'<!--\n  Generated template for the MyPetsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <span class="icon-menu"></span>\n    </button>\n    <ion-title>{{ \'APP_MENU.MY_PETS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="slidesWrapper">\n    <ion-slides *ngIf="loadok" class="petpre" pager slidesPerView="4.5">\n      <ion-slide>\n        <pet-preview [add]="true" (click)="addPet()"></pet-preview>\n      </ion-slide>\n      <ion-slide *ngFor="let pet of pets">\n        <pet-preview [pet]="pet" [isDetail]="true" [isEdit]="true" [isMe]="true" [add]="false"></pet-preview>\n      </ion-slide>\n    </ion-slides>\n  </div>\n\n  <div *ngIf="addPetForm">\n    <qr-card [pet]="newPet" [nueva]="true" (change)="qrChange($event)"></qr-card>\n    <div *ngIf="addPetForm && !addPetForm2" class="start_qr">\n      Comienza escaneando el QR para tu mascota\n    </div>\n    <pet-info-form *ngIf="addPetForm2" [pet]="newPet" [isDetail]="true" [isEdit]="true" [isNew]="true"></pet-info-form>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/home/nicolai/bitbucket/blanco/app-mi-pata-develop/app-mi-pata/src/pages/my-pets/my-pets.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__services_pet_service__["a" /* PetService */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
], MyPetsPage);

//# sourceMappingURL=my-pets.js.map

/***/ })

},[301]);
//# sourceMappingURL=main.js.map